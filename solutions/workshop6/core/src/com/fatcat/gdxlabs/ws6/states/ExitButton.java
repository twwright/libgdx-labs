package com.fatcat.gdxlabs.ws6.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.ws6.game.GameWorld;
import com.fatcat.gdxlabs.ws6.states.Button;

/**
 * Created by Tom on 15/02/2015.
 */
public class ExitButton extends Button
{
   public ExitButton(Vector2 position, TextureRegion unselected, TextureRegion selected)
   {
      super(position, unselected, selected);
   }

   @Override
   public void onPress(GameWorld world)
   {
      // Quit the game!
      Gdx.app.exit();
   }
}
