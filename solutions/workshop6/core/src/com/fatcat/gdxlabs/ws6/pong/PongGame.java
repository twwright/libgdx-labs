package com.fatcat.gdxlabs.ws6.pong;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.ws6.game.Collision;
import com.fatcat.gdxlabs.ws6.game.CompositionStateGame;
import com.fatcat.gdxlabs.ws6.game.KeyBindings;
import com.fatcat.gdxlabs.ws6.states.Button;
import com.fatcat.gdxlabs.ws6.states.ExitButton;
import com.fatcat.gdxlabs.ws6.states.MenuState;
import com.fatcat.gdxlabs.ws6.states.StateChangeButton;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tom on 15/02/2015.
 */
public class PongGame extends CompositionStateGame
{
   public static final String MENU_STATE = "menu";
   public static final String GAME_STATE = "game";
   public static final String PAUSE_STATE = "paused";

   public static final String KEY_P1_UP = "playerOneUp";
   public static final String KEY_P1_DOWN = "playerOneDown";
   public static final String KEY_P2_UP = "playerTwoUp";
   public static final String KEY_P2_DOWN = "playerTwoDown";
   public static final String KEY_PAUSE = "pause";

   private Texture mButtonAtlas;
   private Texture mAtlas;
   private Texture mNumbersAtlas;

   private TextureRegion mButtonPlayUnselected;
   private TextureRegion mButtonPlaySelected;
   private TextureRegion mButtonBackUnselected;
   private TextureRegion mButtonBackSelected;
   private TextureRegion mButtonQuitUnselected;
   private TextureRegion mButtonQuitSelected;
   private TextureRegion mRegionAsteroid;
   private TextureRegion mRegionShipOne;
   private TextureRegion mRegionShipTwo;
   private TextureRegion[] mNumbers;

   @Override
   public void create()
   {
      super.create();

      // Create the collision engine
      mCollision = new Collision(Collision.CIRCLE_MODE);

      // Create key bindings from file
      KeyBindings bindings = new KeyBindings();
      bindings.setBinding(KEY_P1_UP, Input.Keys.W);
      bindings.setBinding(KEY_P1_DOWN, Input.Keys.S);
      bindings.setBinding(KEY_P2_UP, Input.Keys.UP);
      bindings.setBinding(KEY_P2_DOWN, Input.Keys.DOWN);
      bindings.setBinding(KEY_PAUSE, Input.Keys.ESCAPE);
      try {
         bindings = KeyBindings.loadFromFile("controls.txt");
      } catch (FileNotFoundException e) {}

      // Recreate game world to use a pong world
      mWorld = new PongWorld(mWorld, bindings);

      // Load the game assets
      mButtonAtlas = new Texture(Gdx.files.internal("buttonAtlas.png"));
      mButtonPlayUnselected = new TextureRegion(mButtonAtlas, 0, 0, 225, 75);
      mButtonPlaySelected = new TextureRegion(mButtonAtlas, 225, 0, 225, 75);
      mButtonBackUnselected = new TextureRegion(mButtonAtlas, 0, 75, 225, 75);
      mButtonBackSelected = new TextureRegion(mButtonAtlas, 225, 75, 225, 75);
      mButtonQuitUnselected = new TextureRegion(mButtonAtlas, 0, 150, 225, 75);
      mButtonQuitSelected = new TextureRegion(mButtonAtlas, 225, 150, 225, 75);
      mAtlas = new Texture(Gdx.files.internal("atlas.png"));
      mRegionShipOne = new TextureRegion(mAtlas, 0, 0, 84, 103);
      mRegionShipTwo = new TextureRegion(mAtlas, 84, 0, 84, 103);
      mRegionAsteroid = new TextureRegion(mAtlas, 0, 103, 98, 96);
      mNumbersAtlas = new Texture(Gdx.files.internal("numbersAtlas.png"));
      mNumbers = new TextureRegion[10];
      for(int i = 0; i < 10; ++i)
      {
         mNumbers[i] = new TextureRegion(mNumbersAtlas, 35 * i, 0, 35, 44);
      }
      initialiseGame();
   }

   @Override
   public void initialiseGame()
   {
      // Create menu state
      List<Button> menuButtons = new ArrayList<Button>();
      MenuState menuState = new MenuState(MENU_STATE, Color.BLACK, menuButtons);
      menuButtons.add(new StateChangeButton(new Vector2(mWorld.size().x/2, mWorld.size().y/2 + 90), mButtonPlayUnselected, mButtonPlaySelected, GAME_STATE));
      menuButtons.add(new ExitButton(new Vector2(mWorld.size().x/2, mWorld.size().y/2), mButtonQuitUnselected, mButtonQuitSelected));
      mStateManager.registerState(menuState);

      // Create game state
      PongState playState = new PongState(GAME_STATE, Color.PURPLE, mRegionAsteroid, mRegionShipOne, mRegionShipTwo, mNumbers);
      mStateManager.registerState(playState);

      // Create pause state
      List<Button> pauseButtons = new ArrayList<Button>();
      MenuState pauseState = new MenuState(PAUSE_STATE, Color.DARK_GRAY, pauseButtons);
      pauseButtons.add(new StateChangeButton(new Vector2(mWorld.size().x/2, mWorld.size().y/2 + 90), mButtonBackUnselected, mButtonBackSelected, GAME_STATE));
      pauseButtons.add(new StateChangeButton(new Vector2(mWorld.size().x/2, mWorld.size().y/2), mButtonQuitUnselected, mButtonQuitSelected, MENU_STATE));
      mStateManager.registerState(pauseState);


      mStateManager.setNewState(MENU_STATE);
   }

   @Override
   public void dispose()
   {
      mButtonAtlas.dispose();
      mAtlas.dispose();
      mNumbersAtlas.dispose();
   }
}
