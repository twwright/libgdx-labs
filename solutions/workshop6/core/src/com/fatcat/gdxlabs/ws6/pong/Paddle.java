package com.fatcat.gdxlabs.ws6.pong;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.ws6.game.*;

/**
 * Created by Tom on 15/02/2015.
 */
public class Paddle extends GameObject implements IDrawable, IUpdateable, ICollidable
{
   public static final float MAX_VELOCITY = 200;
   private Sprite mSprite;
   private Collider mCollider;

   private Vector2 mVelocity = new Vector2(0, 0);
   private Vector2 mAcceleration;
   private String mUpKey;
   private String mDownKey;

   public Paddle(Vector2 position, TextureRegion texture, float radius, Vector2 acceleration, String upKey, String downKey)
   {
      super(position);
      mAcceleration = acceleration;
      mUpKey = upKey;
      mDownKey = downKey;
      mSprite = new Sprite(texture);
      mCollider = new Collider(position.cpy(), radius);
   }

   @Override
   public Collider getCollider()
   {
      return mCollider;
   }

   @Override
   public boolean isSolid() {
      return true;
   }

   @Override
   public boolean isStatic()
   {
      return true;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      // Do nothing
   }

   @Override
   public void draw(SpriteBatch batch, float delta)
   {
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      mSprite.draw(batch);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      PongWorld pongWorld = (PongWorld)world;
      // Move up or down
      if(pongWorld.getKeyBindings().isActionPressed(mUpKey))
      {
         mVelocity.add(mAcceleration.cpy().scl(delta));
      }
      else if(pongWorld.getKeyBindings().isActionPressed(mDownKey))
      {
         mVelocity.add(mAcceleration.cpy().scl(-delta));
      }

      // Cap velocity
      if(mVelocity.len() > MAX_VELOCITY)
         mVelocity.nor().scl(MAX_VELOCITY);

      // Update position
      mPosition.add(mVelocity.cpy().scl(delta));

      // Check position
      if(mPosition.y < 0)
      {
         mPosition.y = 0;
         mVelocity.y = 0;
      }
      else if(mPosition.y > pongWorld.size().y)
      {
         mPosition.y = pongWorld.size().y;
         mVelocity.y = 0;
      }

      // Update collider position
      mCollider.circle().center(mPosition);
   }
}
