package com.fatcat.gdxlabs.ws6.game;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.List;

/**
 * Created by Tom on 15/02/2015.
 */
public class Collision
{
   public static final int CIRCLE_MODE = 0;
   public static final int RECTANGLE_MODE = 1;

   private int mCollisionMode;

   public Collision(int mode)
   {
      mCollisionMode = mode;
   }

   public void collision(List<ICollidable> collideables, GameWorld world)
   {
      for(int i = 0; i < collideables.size(); ++i)
      {
         ICollidable first = collideables.get(i);
         // No need to
         if(((GameObject)first).isActive())
         {
            // Test collideable with EACH OTHER collideable!
            for(int j = i+1; j < collideables.size(); ++j)
            {
               ICollidable second = collideables.get(j);
               // Only perform collision check if at least one is not static, and both active
               if((!first.isStatic() || !second.isStatic()) && ((GameObject)second).isActive())
               {
                  boolean colliding = collisionCheck(first.getCollider(), second.getCollider());
                  if(colliding)
                  {
                     // Do we need to resolve the collision? Check that both are solid
                     if(first.isSolid() && second.isSolid())
                     {
                        // Move colliders to not be colliding anymore!
                        resolveCollision(first, second);
                     }
                     // Call collision method of each collideable - let the game object respond to the collision
                     first.onCollide(second, world);
                     second.onCollide(first, world);
                  }
               }
            }
         }
      }
   }

   public boolean collisionCheck(Collider first, Collider second)
   {
      if(mCollisionMode == CIRCLE_MODE)
         return collisionCheckCircle(first, second);
      else if(mCollisionMode == RECTANGLE_MODE)
         return collisionCheckRect(first, second);
      return false;
   }

   public void resolveCollision(ICollidable first, ICollidable second)
   {
      if(mCollisionMode == CIRCLE_MODE)
         resolveCollisionCircle(first, second);
      else if(mCollisionMode == RECTANGLE_MODE)
         resolveCollisionRect(first, second);
   }

   public void drawCollider(ShapeRenderer renderer, ICollidable collideable)
   {
      if(mCollisionMode == CIRCLE_MODE)
         drawColliderCircle(renderer, collideable);
      else if(mCollisionMode == RECTANGLE_MODE)
         drawColliderRect(renderer, collideable);
   }
   /*
    * RECTANGLE MODE METHODS
    */

   public void resolveCollisionRect(ICollidable firstCollideable, ICollidable secondCollideable)
   {
      Rectangle first = firstCollideable.getCollider().rect(), second = secondCollideable.getCollider().rect();

      Vector2 firstMin = new Vector2(first.getX(), first.getY());
      Vector2 firstMax = new Vector2(first.getX() + first.getWidth(), first.getY() + first.getHeight());
      Vector2 secondMin = new Vector2(second.getX(), second.getY());
      Vector2 secondMax = new Vector2(second.getX() + second.getWidth(), second.getY() + second.getHeight());

      // Distance to separate with 'first' collider moving in the direction specified
      float separateLeft = secondMin.x - firstMax.x;
      float separateRight = secondMax.x - firstMin.x;
      float separateTop = secondMax.y - firstMin.y;
      float separateBottom = secondMin.y - firstMax.y;

      // Determine smallest overlap in each axis
      Vector2 separate = new Vector2(
            Math.abs(separateLeft) < Math.abs(separateRight) ? separateLeft : separateRight,
            Math.abs(separateTop) < Math.abs(separateBottom) ? separateTop : separateBottom
      );

      // Determine smallest overlap axis
      if(Math.abs(separate.x) < Math.abs(separate.y))
         separate.y = 0;
      else
         separate.x = 0;

      // Now use separate vector to resolve collision
      if(firstCollideable.isStatic())
      {
         // Overlap is vector to move first collider - so reverse to move second
         // Move second full distance
         separate.scl(-1);
         second.setX(second.getX() + separate.x);
         second.setY(second.getY() + separate.y);
      }
      else if(secondCollideable.isStatic())
      {
         // Move first full distance
         first.setX(first.getX() + separate.x);
         first.setY(first.getY() + separate.y);
      }
      else
      {
         // Move each collider half distance
         first.setX(first.getX() + separate.x * 0.5f);
         first.setY(first.getY() + separate.y * 0.5f);
         second.setX(second.getX() + -separate.x * 0.5f);
         second.setY(second.getY() + -separate.y * 0.5f);
      }

      // Update game object positions to reflect changes in collider position
      ((GameObject)firstCollideable).position(firstCollideable.getCollider().rect().getCenter(new Vector2()));
      ((GameObject)secondCollideable).position(secondCollideable.getCollider().rect().getCenter(new Vector2()));
   }

   public boolean collisionCheckRect(Collider firstCollider, Collider secondCollider)
   {
      Rectangle first = firstCollider.rect(), second = secondCollider.rect();

      // First max is greater than second min?
      boolean firstGreater = first.getX() + first.getWidth() > second.getX()
            && first.getY() + first.getHeight() > second.getY();

      // Second max is greater than first min?
      boolean secondGreater = second.getX() + second.getWidth() > first.getX()
            && second.getY() + second.getHeight() > first.getY();

      return firstGreater && secondGreater;

      // Using LibGDX Rectangle methods!
      // return first.overlaps(second);
   }

   public void drawColliderRect(ShapeRenderer renderer, ICollidable collideable)
   {
      Rectangle rect = collideable.getCollider().rect();
      renderer.rect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
   }

   /*
    * CIRCLE MODE METHODS
    */
   public boolean collisionCheckCircle(Collider firstCollider, Collider secondCollider)
   {
      Circle first = firstCollider.circle(), second = secondCollider.circle();

      float distance = first.center().dst(second.center());
      float radiusSum = first.radius() + second.radius();
      return distance <= radiusSum;
   }

   public void resolveCollisionCircle(ICollidable firstCollideable, ICollidable secondCollideable)
   {
      Circle first = firstCollideable.getCollider().circle(), second = secondCollideable.getCollider().circle();

      // Calculate how much the circles are overlapping!
      float distance = first.center().dst(second.center());
      float radiusSum = first.radius() + second.radius();
      float overlap = radiusSum - distance;

      // A - B = B -> A (this gives a direction vector from second to first!
      Vector2 direction = first.center().cpy().sub(second.center()).nor();

      // Are both moving in the resolution, or just one (because the other is static)?
      if(firstCollideable.isStatic())
      {
         // Scale by -1 to REVERSE the direction of the vector - need to move AWAY from collision
         direction.scl(-1);
         // Non-static collider (second) moves away from collisoon the FULL distance of overlap
         second.center().add(direction.cpy().scl(overlap));
      }
      else if(secondCollideable.isStatic())
      {
         // Non-static collider (first) moves away from collision the FULL distance of overlap
         first.center().add(direction.cpy().scl(overlap));
      }
      else
      {
         // Each collider moves away from the collision HALF the distance of the overlap
         first.center().add(direction.cpy().scl(overlap * 0.5f));
         second.center().add(direction.cpy().scl(-overlap * 0.5f));
      }

      // Update the game object position to reflect changed collider center (resolution)
      ((GameObject)firstCollideable).position(firstCollideable.getCollider().circle().center());
      ((GameObject)secondCollideable).position(secondCollideable.getCollider().circle().center());
   }

   public void drawColliderCircle(ShapeRenderer renderer, ICollidable collideable)
   {
      Circle circle = collideable.getCollider().circle();
      renderer.circle(circle.center().x, circle.center().y, circle.radius());
   }
}
