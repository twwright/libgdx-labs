package com.fatcat.gdxlabs.ws6.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Tom on 20/01/2015.
 */
public class KeyBindings
{
   Map<String, Integer> mActionMap;

   public KeyBindings()
   {
      mActionMap = new HashMap<String, Integer>();
   }

   public boolean isActionPressed(String action)
   {
      Integer key = mActionMap.get(action);
      if(key == null)
         return false;
      return Gdx.input.isKeyPressed(key);
   }

   public boolean isActionJustPressed(String action)
   {
      Integer key = mActionMap.get(action);
      if(key == null)
         return false;
      return Gdx.input.isKeyJustPressed(key);
   }

   public void setBinding(String action, int key)
   {
      mActionMap.put(action, key);
   }

   public static KeyBindings loadFromFile(String filename) throws FileNotFoundException
   {
      // Open bindings file for input
      Scanner in = new Scanner(Gdx.files.internal(filename).file());
      KeyBindings bindings = new KeyBindings();
      while(in.hasNextLine())
      {
         // Retrieve a binding from the file
         String[] binding = in.nextLine().split("="); // eg. Jump=SPACE
         try
         {
            // Use reflection to retrieve matching field from Keys class
            // ie. Retrieve Keys.SPACE field
            Field keyField = Input.Keys.class.getField(binding[1]);
            // Retrieve value of field (null as parameter as no instance)
            int keyConstant = keyField.getInt(null);
            // Add new binding to KeyBindings object
            bindings.setBinding(binding[0], keyConstant);
         }
         catch (Exception e)
         {
            // Error getting the constant, ignore this binding
            e.printStackTrace();
         }
      }
      return bindings;
   }

}
