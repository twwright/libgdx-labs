package com.fatcat.gdxlabs.ws6.game;

/**
 * Created by Tom on 11/02/2015.
 */
public interface IUpdateable
{
   public void update(GameWorld world, float delta);
}
