package com.fatcat.gdxlabs.ws6.game;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tom on 12/02/2015.
 */
public class Circle
{
   public Vector2 mCenter;
   public float mRadius;

   public Circle(Vector2 center, float radius)
   {
      mCenter = center;
      mRadius = radius;
   }

   public Vector2 center()
   {
      return mCenter;
   }

   public Vector2 center(Vector2 newCenter)
   {
      mCenter.set(newCenter);
      return mCenter;
   }

   public float radius()
   {
      return mRadius;
   }

   public float radius(float newRadius)
   {
      mRadius = newRadius;
      return mRadius;
   }
}
