package com.fatcat.gdxlabs.ws6.pong;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.ws6.game.GameWorld;
import com.fatcat.gdxlabs.ws6.game.KeyBindings;
import com.fatcat.gdxlabs.ws6.states.StateManager;

/**
 * Created by Tom on 15/02/2015.
 */
public class PongWorld extends GameWorld
{
   private int mPlayer1Score = 0;
   private int mPlayer2Score = 0;

   private KeyBindings mBindings;

   public PongWorld(Vector2 size, Camera camera, StateManager manager, KeyBindings bindings)
   {
      super(size, camera, manager);
      mBindings = bindings;
   }

   public PongWorld(GameWorld world, KeyBindings bindings)
   {
      this(world.size(), world.getCamera(), world.getStateManager(), bindings);
   }

   public int getPlayer1Score()
   {
      return mPlayer1Score;
   }

   public int getPlayer2Score()
   {
      return mPlayer2Score;
   }

   public void setPlayer1Score(int score)
   {
      mPlayer1Score = score;
   }

   public void setPlayer2Score(int score)
   {
      mPlayer2Score = score;
   }

   public KeyBindings getKeyBindings()
   {
      return mBindings;
   }
}
