package com.fatcat.gdxlabs.ws6.pong;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.ws6.game.GameObject;
import com.fatcat.gdxlabs.ws6.game.GameWorld;
import com.fatcat.gdxlabs.ws6.states.State;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tom on 15/02/2015.
 */
public class PongState extends State
{
   private Color mBackground;
   private List<GameObject> mStoredGameObjects;

   private TextureRegion mRegionBall;
   private TextureRegion mRegionPlayerOne;
   private TextureRegion mRegionPlayerTwo;
   private TextureRegion[] mNumbers;

   private TextureRegion mRegionScoreOne;
   private Vector2 mScoreOnePos;
   private TextureRegion mRegionScoreTwo;
   private Vector2 mScoreTwoPos;

   public PongState(String name, Color background, TextureRegion ball, TextureRegion playerOne, TextureRegion playerTwo, TextureRegion[] numbers)
   {
      super(name);
      mBackground = background;
      mRegionBall = ball;
      mRegionPlayerOne = playerOne;
      mRegionPlayerTwo = playerTwo;
      mNumbers = numbers;
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // We know the game world being used is a pong world
      PongWorld pongworld = (PongWorld)world;

      // Clear the screen with our background color!
      Gdx.gl.glClearColor(mBackground.r, mBackground.g, mBackground.b, mBackground.a);
      Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

      if(pongworld.getPlayer1Score() >= 9 || pongworld.getPlayer2Score() >= 9)
         pongworld.getStateManager().setNewState(PongGame.MENU_STATE);

      mRegionScoreOne = mNumbers[pongworld.getPlayer1Score()];
      mRegionScoreTwo = mNumbers[pongworld.getPlayer2Score()];

      // Check pausing
      if(pongworld.getKeyBindings().isActionJustPressed(PongGame.KEY_PAUSE))
         world.getStateManager().setNewState(PongGame.PAUSE_STATE);
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      // Draw scores
      float offsetX = mRegionScoreOne.getRegionWidth()/2, offsetY = mRegionScoreTwo.getRegionHeight()/2;
      batch.draw(mRegionScoreOne, mScoreOnePos.x-offsetX, mScoreOnePos.y-offsetY);
      batch.draw(mRegionScoreTwo, mScoreTwoPos.x-offsetX, mScoreTwoPos.y-offsetY);
   }

   @Override
   public void enter(State oldState, GameWorld world)
   {
      // Restore game objects or create new ones
      if(oldState.getName().equals(PongGame.PAUSE_STATE))
      {
         for(GameObject object : mStoredGameObjects)
            world.add(object);
      }
      else
      {
         PongWorld pongWorld = (PongWorld)world;

         // Set score position
         mScoreOnePos = new Vector2(120, world.size().y/2);
         mScoreTwoPos = new Vector2(world.size().x-120, world.size().y/2);

         // Create player objects
         Vector2 acceleration = new Vector2(0, 300);
         Paddle playerOne = new Paddle(new Vector2(50, world.size().y/2), mRegionPlayerOne, mRegionPlayerOne.getRegionWidth()/2, acceleration, PongGame.KEY_P1_UP, PongGame.KEY_P1_DOWN);
         Paddle playerTwo = new Paddle(new Vector2(world.size().x-50, world.size().y/2), mRegionPlayerTwo, mRegionPlayerTwo.getRegionWidth()/2, acceleration, PongGame.KEY_P2_UP, PongGame.KEY_P2_DOWN);

         // Create ball
         Vector2 velocity = new Vector2(Math.random() > 0.5f ? 150 : -150, 0);
         Ball ball = new Ball(new Vector2(world.size().x/2, world.size().y/2), mRegionBall, mRegionBall.getRegionWidth()/2, velocity, 1.10f);

         // Add objects to world
         world.add(ball);
         world.add(playerOne);
         world.add(playerTwo);

         // Reset scores if we are starting a new game
         if(oldState.getName().equals(PongGame.MENU_STATE))
         {
            // Reset scores
            pongWorld.setPlayer1Score(0);
            pongWorld.setPlayer2Score(0);
         }

      }

      // Clear stored game objects
      if(mStoredGameObjects != null)
      {
         mStoredGameObjects.clear();
         mStoredGameObjects = null;
      }
   }

   @Override
   public void exit(State newState, GameWorld world)
   {
      // Store game objects in case we return from paused
      mStoredGameObjects = new ArrayList<GameObject>(world.getGameObjects());
      // Empty the world of our game objects
      world.removeAll();
   }
}
