package com.fatcat.gdxlabs.ws6.states;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.ws6.game.GameWorld;

/**
 * Created by Tom on 15/02/2015.
 */
public class StateChangeButton extends Button
{
   private String mNewStateName;
   public StateChangeButton(Vector2 position, TextureRegion unselected, TextureRegion selected, String stateName)
   {
      super(position, unselected, selected);
      mNewStateName = stateName;
   }

   @Override
   public void onPress(GameWorld world)
   {
      world.getStateManager().setNewState(mNewStateName);
   }
}
