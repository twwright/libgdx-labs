package com.fatcat.gdxlabs.ws6.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.fatcat.gdxlabs.ws6.game.GameObject;
import com.fatcat.gdxlabs.ws6.game.GameWorld;
import com.fatcat.gdxlabs.ws6.game.IDrawable;
import com.fatcat.gdxlabs.ws6.game.IUpdateable;

/**
 * Created by Tom on 15/02/2015.
 */
public abstract class Button extends GameObject implements IUpdateable, IDrawable
{
   private Sprite mSprite;
   private TextureRegion mRegionUnselected;
   private TextureRegion mRegionSelected;
   private boolean mSelected = false;

   public Button(Vector2 position, TextureRegion unselected, TextureRegion selected)
   {
      super(position);
      mRegionUnselected = unselected;
      mRegionSelected = selected;
      mSprite = new Sprite(unselected);
      mSprite.setPosition(position.x - mSprite.getWidth()/2, position.y - mSprite.getHeight()/2);
   }

   public void setSelected(boolean selected)
   {
      mSelected = selected;
      mSprite.setRegion(mSelected ? mRegionSelected : mRegionUnselected);
   }


   @Override
   public void draw(SpriteBatch batch, float delta)
   {
      mSprite.draw(batch);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      if(Gdx.input.isTouched())
      {
         // Retrieve touch coordinates and unproject with camera
         Vector3 screenCoords = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
         world.getCamera().unproject(screenCoords);

         Vector2 worldCoords = new Vector2(screenCoords.x, screenCoords.y);

         // Check if transformed screen coordinates are inside the button sprite
         Rectangle rect = mSprite.getBoundingRectangle();
         this.setSelected(rect.contains(worldCoords));
      }
      else
      {
         if(mSelected)
         {
            onPress(world);
            mSelected = false;
         }
      }
   }

   public abstract void onPress(GameWorld world);
}
