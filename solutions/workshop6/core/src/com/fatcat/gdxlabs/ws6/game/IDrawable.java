package com.fatcat.gdxlabs.ws6.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Tom on 11/02/2015.
 */
public interface IDrawable
{
   public void draw(SpriteBatch batch, float delta);
}
