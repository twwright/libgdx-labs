package com.fatcat.gdxlabs.ws6.pong;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.ws6.game.*;

/**
 * Created by Tom on 15/02/2015.
 */
public class Ball extends GameObject implements IDrawable, IUpdateable, ICollidable
{
   private Collider mCollider;
   private Sprite mSprite;

   private Vector2 mVelocity;
   private float mVelocityMultiply;
   private float mRotationSpeed;

   public Ball(Vector2 position, TextureRegion texture, float radius, Vector2 velocity, float velocityMultiply)
   {
      super(position);
      mVelocity = velocity;
      mVelocityMultiply = velocityMultiply;
      mRotationSpeed = velocity.len();
      mSprite = new Sprite(texture);
      mCollider = new Collider(position.cpy(), radius);
   }

   @Override
   public Collider getCollider()
   {
      return mCollider;
   }

   @Override
   public boolean isSolid()
   {
      return true;
   }

   @Override
   public boolean isStatic()
   {
      return false;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      // Circle collision, so velocity direction is to be away from collision
      Vector2 dir = mPosition.cpy().sub(((GameObject)other).position()).nor();
      // Scale direction to magnitude of current velocity times the multiplier
      dir.scl(mVelocity.len() * mVelocityMultiply);
      mVelocity = dir;

      mRotationSpeed = -mRotationSpeed * mVelocityMultiply;
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // We know the game world being used is a pong world
      PongWorld pongWorld = (PongWorld)world;

      // Modify sprite rotation
      mSprite.rotate(mRotationSpeed * delta);

      // Modify position
      mPosition.add(mVelocity.cpy().scl(delta));

      // Wrap around vertically
      if(mPosition.y > world.size().y)
         mPosition.y = 0;
      else if(mPosition.y < 0)
         mPosition.y = world.size().y;

      // Check for win on either side!
      if(mPosition.x > world.size().x)
      {
         pongWorld.setPlayer1Score(pongWorld.getPlayer1Score() + 1);
         pongWorld.getStateManager().setNewState(PongGame.GAME_STATE);
      }
      else if(mPosition.x < 0)
      {
         pongWorld.setPlayer2Score(pongWorld.getPlayer2Score() + 1);
         pongWorld.getStateManager().setNewState(PongGame.GAME_STATE);
      }

      // Update collider position
      mCollider.circle().center(mPosition);
   }

   @Override
   public void draw(SpriteBatch batch, float delta)
   {
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);

      mSprite.draw(batch);
   }
}
