package com.fatcat.gdxlabs.ws5.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.fatcat.gdxlabs.ws5.asteroids.AsteroidsGame;
import com.fatcat.gdxlabs.ws5.platformer.PlatformerGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 800;
		config.height = 480;
		new LwjglApplication(new AsteroidsGame(), config);
	}
}
