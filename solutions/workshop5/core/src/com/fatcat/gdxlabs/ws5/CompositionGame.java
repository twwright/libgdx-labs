package com.fatcat.gdxlabs.ws5;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.List;

/**
 * Created by Tom on 12/02/2015.
 */
public abstract class CompositionGame extends ApplicationAdapter
{
   protected SpriteBatch mBatch;
   protected ShapeRenderer mRenderer; // For drawing primitives - used to debug the colliders - see below
   protected Camera mCamera;
   protected GameWorld mWorld;

   @Override
   public void create ()
   {
      mBatch = new SpriteBatch();
      mRenderer = new ShapeRenderer();
      mCamera = new OrthographicCamera(800, 480);
      mCamera.position.set(mCamera.viewportWidth / 2, mCamera.viewportHeight / 2, 0);
   }

   @Override
   public void render ()
   {
      Gdx.gl.glClearColor(0.4f, 0.2f, 0.5f, 1);
      Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

      if(Gdx.input.isKeyJustPressed(Input.Keys.F1) || !mWorld.isRunning())
         initialiseGame(); // Reset

      // Updates
      update();

      // Collision
      collision();

      // Update camera
      mCamera.update();

      // Rendering
      draw();
   }

   public void update()
   {
      List<IUpdateable> updateables = mWorld.getUpdateables();
      for(IUpdateable updateable : updateables)
      {
         if(((GameObject)updateable).isActive())
            updateable.update(mWorld, Gdx.graphics.getDeltaTime());
      }
      mWorld.completeUpdates();
   }

   public void draw()
   {
      List<IDrawable> drawables = mWorld.getDrawables();
      mBatch.setProjectionMatrix(mCamera.combined);
      mBatch.begin();
      for(IDrawable drawable : drawables)
      {
         if(((GameObject)drawable).isActive())
            drawable.draw(mBatch, Gdx.graphics.getDeltaTime());
      }
      mBatch.end();

      // Debug drawing of colliders
      mRenderer.setProjectionMatrix(mCamera.combined);
      mRenderer.begin(ShapeRenderer.ShapeType.Line);
      for(ICollidable collideable : mWorld.getCollideables())
      {
         if(((GameObject)collideable).isActive())
            drawCollider(mRenderer, collideable);
      }
      mRenderer.end();
   }

   public void collision()
   {
      List<ICollidable> collideables = mWorld.getCollideables();
      for(int i = 0; i < collideables.size(); ++i)
      {
         ICollidable first = collideables.get(i);
         // No need to
         if(((GameObject)first).isActive())
         {
            // Test collideable with EACH OTHER collideable!
            for(int j = i+1; j < collideables.size(); ++j)
            {
               ICollidable second = collideables.get(j);
               // Only perform collision check if at least one is not static, and both active
               if((!first.isStatic() || !second.isStatic()) && ((GameObject)second).isActive())
               {
                  boolean colliding = collisionCheck(first.getCollider(), second.getCollider());
                  if(colliding)
                  {
                     // Do we need to resolve the collision? Check that both are solid
                     if(first.isSolid() && second.isSolid())
                     {
                        // Move colliders to not be colliding anymore!
                        resolveCollision(first, second);
                     }
                     // Call collision method of each collideable - let the game object respond to the collision
                     first.onCollide(second, mWorld);
                     second.onCollide(first, mWorld);
                  }
               }
            }
         }
      }
   }

   public abstract void initialiseGame();
   public abstract boolean collisionCheck(Collider first, Collider second);
   public abstract void resolveCollision(ICollidable first, ICollidable second);
   public abstract void drawCollider(ShapeRenderer renderer, ICollidable collideable);
}
