package com.fatcat.gdxlabs.ws5.platformer;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.ws5.*;
import com.fatcat.gdxlabs.ws5.Collider;

/**
 * Created by Tom on 12/02/2015.
 */
public class Block extends GameObject implements IDrawable, ICollidable
{
   private Sprite mSprite;
   private Collider mCollider;

   public Block(TextureRegion textureRegion, Vector2 position)
   {
      mPosition.set(position);
      mSprite = new Sprite(textureRegion);
      mSprite.setPosition(mPosition.x, mPosition.y);
      mCollider = new Collider(mSprite.getBoundingRectangle());
   }
   @Override
   public Collider getCollider()
   {
      return mCollider;
   }

   @Override
   public boolean isSolid()
   {
      return true;
   }

   @Override
   public boolean isStatic()
   {
      return true;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      // Do nothing
   }

   @Override
   public void draw(SpriteBatch batch, float delta)
   {
      mSprite.draw(batch);
   }
}
