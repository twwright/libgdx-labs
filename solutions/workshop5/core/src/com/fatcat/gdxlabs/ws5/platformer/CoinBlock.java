package com.fatcat.gdxlabs.ws5.platformer;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.ws5.*;

/**
 * Created by Tom on 12/02/2015.
 */
public class CoinBlock extends GameObject implements ICollidable, IDrawable
{
   private Sprite mSprite;
   private TextureRegion mRegionUnhit;
   private TextureRegion mRegionHit;
   private Collider mCollider;

   public CoinBlock(TextureRegion unhit, TextureRegion hit, Vector2 position)
   {
      mPosition.set(position);
      mSprite = new Sprite(unhit);
      mSprite.setPosition(position.x, position.y);
      mRegionHit = hit;
      mRegionUnhit = unhit;
      mCollider = new Collider(mSprite.getBoundingRectangle());
   }
   @Override
   public Collider getCollider()
   {
      return mCollider;
   }

   @Override
   public boolean isSolid()
   {
      return true;
   }

   @Override
   public boolean isStatic()
   {
      return true;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      // Check that we have been hit by the player
      if(other instanceof Player)
      {
         // Determine direction of collision and set velocity in that axis to 0
         Vector2 otherPos = ((GameObject)other).position();
         Vector2 directionToOther = otherPos.cpy().sub(mPosition).nor();
         Vector2 down = new Vector2(0, -1);

         // How similar is the direction to down
         float dot = directionToOther.dot(down);

         // If dot is close to 1 (close to directly down, change to pressed
         if(dot > 0.75f)
            mSprite.setRegion(mRegionHit);
      }
   }

   @Override
   public void draw(SpriteBatch batch, float delta)
   {
      mSprite.draw(batch);
   }
}
