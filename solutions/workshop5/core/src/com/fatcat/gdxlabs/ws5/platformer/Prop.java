package com.fatcat.gdxlabs.ws5.platformer;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.ws5.*;

/**
 * Created by Tom on 12/02/2015.
 */
public class Prop extends GameObject implements IDrawable
{
   private Sprite mSprite;

   public Prop(TextureRegion texture, Vector2 position)
   {
      mPosition.set(position);
      mSprite = new Sprite(texture);
      mSprite.setPosition(mPosition.x, mPosition.y);
   }

   @Override
   public void draw(SpriteBatch batch, float delta)
   {
      mSprite.draw(batch);
   }
}
