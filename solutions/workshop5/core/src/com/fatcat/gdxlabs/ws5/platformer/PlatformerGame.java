package com.fatcat.gdxlabs.ws5.platformer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.ws5.*;


public class PlatformerGame extends CompositionGame
{
	public static final int NUM_STARS = 10;
	public static final int NUM_ASTEROIDS = 5;

	Texture mAtlas;

	TextureRegion mRegionPlayer;
	TextureRegion mRegionPlayerJump;
	TextureRegion mRegionPlayerHurt;
	TextureRegion mRegionEnemy;
	TextureRegion mRegionGrass;
	TextureRegion mRegionDirt;
	TextureRegion mRegionFence;
	TextureRegion mRegionPlant;
	TextureRegion mRegionCoin;

	@Override
	public void create ()
	{
		super.create();
		mAtlas = new Texture(Gdx.files.internal("atlas2.png"));

		mRegionPlayer = new TextureRegion(mAtlas, 0, 0, 66, 92);
		mRegionPlayerJump = new TextureRegion(mAtlas, 0, 92, 67, 94);
		mRegionPlayerHurt = new TextureRegion(mAtlas, 67, 70, 69, 92);
		mRegionGrass = new TextureRegion(mAtlas, 66, 0, 70, 70);
		mRegionDirt = new TextureRegion(mAtlas, 136, 0, 70, 70);
		mRegionCoin = new TextureRegion(mAtlas, 206, 0, 36, 36);
		mRegionFence = new TextureRegion(mAtlas, 136, 70, 70, 61);
		mRegionPlant = new TextureRegion(mAtlas, 206, 70, 48, 106);
		mRegionEnemy = new TextureRegion(mAtlas, 136, 131, 70, 39);

		initialiseGame();
	}

	public void initialiseGame()
	{
		mWorld = new GameWorld(new Vector2(mCamera.viewportWidth, mCamera.viewportHeight));

		// Props
		mWorld.add(new Prop(mRegionPlant, new Vector2(155, 110)));
		mWorld.add(new Prop(mRegionFence, new Vector2(665, 110)));

		// Blocks
		mWorld.add(new Block(mRegionGrass, new Vector2(-70, 50)));
		mWorld.add(new Block(mRegionGrass, new Vector2(0, 50)));
		mWorld.add(new Block(mRegionGrass, new Vector2(70, 50)));
		mWorld.add(new Block(mRegionGrass, new Vector2(140, 50)));

		mWorld.add(new Block(mRegionDirt, new Vector2(-35, 120)));
		mWorld.add(new Block(mRegionDirt, new Vector2(-35, 190)));
		mWorld.add(new Block(mRegionGrass, new Vector2(-35, 260)));

		mWorld.add(new Block(mRegionGrass, new Vector2(350, 50)));

		mWorld.add(new Block(mRegionDirt, new Vector2(560, 50)));
		mWorld.add(new Block(mRegionGrass, new Vector2(560, 120)));
		mWorld.add(new Block(mRegionGrass, new Vector2(630, 50)));
		mWorld.add(new Block(mRegionGrass, new Vector2(700, 50)));
		mWorld.add(new Block(mRegionGrass, new Vector2(770, 50)));
		mWorld.add(new Block(mRegionGrass, new Vector2(840, 50)));
		mWorld.add(new Block(mRegionGrass, new Vector2(910, 50)));
		mWorld.add(new Block(mRegionGrass, new Vector2(945, 120)));
		mWorld.add(new Block(mRegionGrass, new Vector2(980, 50)));
		mWorld.add(new Block(mRegionGrass, new Vector2(1050, 50)));
		mWorld.add(new Block(mRegionGrass, new Vector2(1120, 50)));

		// Enemies
		mWorld.add(new Enemy(mRegionEnemy, new Vector2(800, 150), new Vector2(-50, 0)));
		mWorld.add(new Enemy(mRegionEnemy, new Vector2(400, 130), new Vector2(-50, 0)));

		// Player
		mWorld.add(new Player(mRegionPlayer, mRegionPlayerJump, mRegionPlayerHurt, new Vector2(140, 200), mCamera));

	}

	@Override
	public void resolveCollision(ICollidable firstCollideable, ICollidable secondCollideable)
	{
		Rectangle first = firstCollideable.getCollider().rect(), second = secondCollideable.getCollider().rect();

		Vector2 firstMin = new Vector2(first.getX(), first.getY());
		Vector2 firstMax = new Vector2(first.getX() + first.getWidth(), first.getY() + first.getHeight());
		Vector2 secondMin = new Vector2(second.getX(), second.getY());
		Vector2 secondMax = new Vector2(second.getX() + second.getWidth(), second.getY() + second.getHeight());

		// Distance to separate with 'first' collider moving in the direction specified
		float separateLeft = secondMin.x - firstMax.x;
		float separateRight = secondMax.x - firstMin.x;
		float separateTop = secondMax.y - firstMin.y;
		float separateBottom = secondMin.y - firstMax.y;

		/*
		separateLeft = rect2.getX() - (rect1.getX() + rect1.getWidth());
		separateRight = (rect2.getX() + rect2.getWidth()) - rect1.getX();
		separateTop = (rect2.getY() + rect2.getHeight()) - rect1.getY();
		separateBottom = rect2.getY() - (rect1.getY() + rect1.getHeight());

		// Distance to separate with 'first' collider moving in the direction specified
		float separateLeft, separateRight, separateTop, separateBottom;
		separateLeft = rect2.getX() - (rect1.getX() + rect1.getWidth());
		separateRight = (rect2.getX() + rect2.getWidth()) - rect1.getX();
		separateTop = (rect2.getY() + rect2.getHeight()) - rect1.getY();
		separateBottom = rect2.getY() - (rect1.getY() + rect1.getHeight());
		*/

		// Determine smallest overlap in each axis
		Vector2 separate = new Vector2(
				Math.abs(separateLeft) < Math.abs(separateRight) ? separateLeft : separateRight,
				Math.abs(separateTop) < Math.abs(separateBottom) ? separateTop : separateBottom
		);

		// Determine smallest overlap axis
		if(Math.abs(separate.x) < Math.abs(separate.y))
			separate.y = 0;
		else
			separate.x = 0;

		// Now use separate vector to resolve collision
		if(firstCollideable.isStatic())
		{
			// Overlap is vector to move first collider - so reverse to move second
			// Move second full distance
			separate.scl(-1);
			second.setX(second.getX() + separate.x);
			second.setY(second.getY() + separate.y);
		}
		else if(secondCollideable.isStatic())
		{
			// Move first full distance
			first.setX(first.getX() + separate.x);
			first.setY(first.getY() + separate.y);
		}
		else
		{
			// Move each collider half distance
			first.setX(first.getX() + separate.x * 0.5f);
			first.setY(first.getY() + separate.y * 0.5f);
			second.setX(second.getX() + -separate.x * 0.5f);
			second.setY(second.getY() + -separate.y * 0.5f);
		}

		// Update game object positions to reflect changes in collider position
		((GameObject)firstCollideable).position(firstCollideable.getCollider().rect().getCenter(new Vector2()));
		((GameObject)secondCollideable).position(secondCollideable.getCollider().rect().getCenter(new Vector2()));
	}

	@Override
	public boolean collisionCheck(Collider firstCollider, Collider secondCollider)
	{
		Rectangle first = firstCollider.rect(), second = secondCollider.rect();

		// First max is greater than second min?
		boolean firstGreater = first.getX() + first.getWidth() > second.getX()
									&& first.getY() + first.getHeight() > second.getY();

		// Second max is greater than first min?
		boolean secondGreater = second.getX() + second.getWidth() > first.getX()
									&& second.getY() + second.getHeight() > first.getY();

		return firstGreater && secondGreater;

		// Using LibGDX Rectangle methods!
		// return first.overlaps(second);
	}

	@Override
	public void drawCollider(ShapeRenderer renderer, ICollidable collideable)
	{
			Rectangle rect = collideable.getCollider().rect();
			renderer.rect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
	}

}
