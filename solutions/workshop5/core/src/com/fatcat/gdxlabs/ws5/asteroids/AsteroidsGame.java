package com.fatcat.gdxlabs.ws5.asteroids;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.ws5.*;

public class AsteroidsGame extends CompositionGame
{
	public static final int NUM_STARS = 10;
	public static final int NUM_ASTEROIDS = 5;
	Texture mAtlas;

	TextureRegion mRegionShip;
	TextureRegion mRegionAsteroidL;
	TextureRegion mRegionStar;

	@Override
	public void create ()
	{
		super.create();
		mAtlas = new Texture(Gdx.files.internal("atlas.png"));

		mRegionShip = new TextureRegion(mAtlas, 0, 0, 83, 103);
		mRegionAsteroidL = new TextureRegion(mAtlas, 83, 0, 98, 96);
		mRegionStar = new TextureRegion(mAtlas, 181, 40, 8, 8);

		initialiseGame();
	}

	public void initialiseGame()
	{
		mWorld = new GameWorld(new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
		for(int i = 0; i < NUM_STARS; ++i)
		{
			// Randomise position and velocity
			Vector2 position = new Vector2((float)Math.random() * Gdx.graphics.getWidth(), (float)Math.random() * Gdx.graphics.getHeight());
			Star star = new Star(mRegionStar, position);
			// Add star gameobject to world
			mWorld.add(star);
		}
		for(int i = 0; i < NUM_ASTEROIDS; ++i)
		{
			// Randomise position and velocity
			Vector2 position = new Vector2((float)Math.random() * Gdx.graphics.getWidth(), (float)Math.random() * Gdx.graphics.getHeight());
			Vector2 velocity = new Vector2(-50 + (float)Math.random() * 100, -50 + (float)Math.random());
			Asteroid asteroid = new Asteroid(mRegionAsteroidL, position, mRegionAsteroidL.getRegionWidth()/2, velocity.x * 2, velocity);
			// Add asteroid gameobject to world
			mWorld.add(asteroid);
		}
	}

	@Override
	public boolean collisionCheck(Collider firstCollider, Collider secondCollider)
	{
		Circle first = firstCollider.circle(), second = secondCollider.circle();

		float distance = first.center().dst(second.center());
		float radiusSum = first.radius() + second.radius();
		return distance <= radiusSum;
	}
	@Override
	public void resolveCollision(ICollidable firstCollideable, ICollidable secondCollideable)
	{
		Circle first = firstCollideable.getCollider().circle(), second = secondCollideable.getCollider().circle();

		// Calculate how much the circles are overlapping!
		float distance = first.center().dst(second.center());
		float radiusSum = first.radius() + second.radius();
		float overlap = radiusSum - distance;

		// A - B = B -> A (this gives a direction vector from second to first!
		Vector2 direction = first.center().cpy().sub(second.center()).nor();

		// Are both moving in the resolution, or just one (because the other is static)?
		if(firstCollideable.isStatic())
		{
			// Scale by -1 to REVERSE the direction of the vector - need to move AWAY from collision
			direction.scl(-1);
			// Non-static collider (second) moves away from collisoon the FULL distance of overlap
			second.center().add(direction.cpy().scl(overlap));
		}
		else if(secondCollideable.isStatic())
		{
			// Non-static collider (first) moves away from collision the FULL distance of overlap
			first.center().add(direction.cpy().scl(overlap));
		}
		else
		{
			// Each collider moves away from the collision HALF the distance of the overlap
			first.center().add(direction.cpy().scl(overlap * 0.5f));
			second.center().add(direction.cpy().scl(-overlap * 0.5f));
		}

		// Update the game object position to reflect changed collider center (resolution)
		((GameObject)firstCollideable).position(firstCollideable.getCollider().circle().center());
		((GameObject)secondCollideable).position(secondCollideable.getCollider().circle().center());
	}

	@Override
	public void drawCollider(ShapeRenderer renderer, ICollidable collideable)
	{
		Circle circle = collideable.getCollider().circle();
		renderer.circle(circle.center().x, circle.center().y, circle.radius());
	}
}
