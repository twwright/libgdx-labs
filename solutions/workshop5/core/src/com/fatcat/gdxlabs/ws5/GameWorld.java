package com.fatcat.gdxlabs.ws5;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tom on 11/02/2015.
 */
public class GameWorld
{
   // Main collection of all game objects
   private List<GameObject> mGameObjects = new ArrayList<GameObject>();

   // Collections to store game objects to add or remove
   private List<GameObject> mAddedGameObjects = new LinkedList<GameObject>();
   private List<GameObject> mRemovedGameObjects = new LinkedList<GameObject>();

   // Specific collection of each game object composition element
   private List<IUpdateable> mUpdateables = new ArrayList<IUpdateable>();
   private List<ICollidable> mCollideables = new ArrayList<ICollidable>();
   private List<IDrawable> mDrawables = new ArrayList<IDrawable>();

   private boolean mIsRunning = true;

   // Game world size
   private Vector2 mWorldSize;

   public GameWorld(Vector2 size)
   {
      mWorldSize = size;
   }

   // THIS METHOD CANNOT BE USED DURING UPDATE LOOP AS IT MODIFIES THE UPDATEABLES COLLECTION
   public void add(GameObject obj)
   {
      mGameObjects.add(obj);
      if(obj instanceof IUpdateable)
         mUpdateables.add((IUpdateable)obj);
      if(obj instanceof ICollidable)
         mCollideables.add((ICollidable)obj);
      if(obj instanceof IDrawable)
         mDrawables.add((IDrawable)obj);
   }

   /*
    * Unused in these examples, but given as a reference!
    * THIS METHOD CANNOT BE USED DURING UPDATE LOOP AS IT MODIFIES THE UPDATEABLES COLLECTION
    */
   public void remove(GameObject obj)
   {
      mGameObjects.remove(obj);

      if(obj instanceof IUpdateable)
         mUpdateables.remove(obj);
      if(obj instanceof ICollidable)
         mCollideables.remove(obj);
      if(obj instanceof IDrawable)
         mDrawables.remove(obj);
   }

   /*
    * These methods unused in these examples, but given as a reference!
    */
   public void updateRemove(GameObject obj)
   {
      if(!mRemovedGameObjects.contains(obj))
         mRemovedGameObjects.add(obj);
   }

   public void updateAdd(GameObject obj)
   {
      if(!mRemovedGameObjects.contains(obj))
         mRemovedGameObjects.add(obj);
   }

   public void completeUpdates()
   {
      for(GameObject obj : mRemovedGameObjects)
         remove(obj);
      mRemovedGameObjects.clear();
      for(GameObject obj : mAddedGameObjects)
         add(obj);
      mAddedGameObjects.clear();
   }

   public List<IUpdateable> getUpdateables()
   {
      return mUpdateables;
   }

   public List<ICollidable> getCollideables()
   {
      return mCollideables;
   }

   public List<IDrawable> getDrawables()
   {
      return mDrawables;
   }

   public Vector2 size()
   {
      return mWorldSize;
   }

   public void setRunning(boolean running)
   {
      mIsRunning = running;
   }

   public boolean isRunning()
   {
      return mIsRunning;
   }
}
