package com.fatcat.gdxlabs.ws5.asteroids;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.ws5.*;

/**
 * Created by Tom on 12/02/2015.
 */
public class Asteroid extends GameObject implements IUpdateable, ICollidable, IDrawable
{
   // Visual element for drawing
   private Sprite mSprite;
   // Collider - using Circle
   private Collider mCollider;

   private float mRotationSpeed;
   private Vector2 mVelocity;

   public Asteroid(TextureRegion texture, Vector2 position, float radius, float rotationSpeed, Vector2 initialVelocity)
   {
      super(position);
      mSprite = new Sprite(texture);
      mCollider = new Collider(mPosition.cpy(), radius);
      mRotationSpeed = rotationSpeed;
      mVelocity = initialVelocity;
   }

   @Override
   public Collider getCollider()
   {
      return mCollider;
   }

   @Override
   public boolean isSolid() {
      return true;
   }

   @Override
   public boolean isStatic()
   {
      return false;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      // Circle collision, so velocity direction is to be away from collision
      Vector2 dir = mPosition.cpy().sub(((GameObject) other).position()).nor();

      // Scale direction to magnitude of current velocity
      dir.scl(mVelocity.len());
      mVelocity = dir;

      // Reverse rotation
      mRotationSpeed = -mRotationSpeed;
   }

   @Override
   public void draw(SpriteBatch batch, float delta)
   {
      // Update sprite position to match that of gameobject!
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      // Draw sprite using provided sprite batch
      mSprite.draw(batch);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // Update rotation of sprite
      mSprite.rotate(mRotationSpeed * delta);

      // Apply velocity to position
      mPosition.add(mVelocity.cpy().scl(delta));

      // Check position (camera wrap-around)
      if(mPosition.x < 0)
         mPosition.x = world.size().x;
      else if(mPosition.x > world.size().x)
         mPosition.x = 0;

      if(mPosition.y < 0)
         mPosition.y = world.size().y;
      else if(mPosition.y > world.size().y)
         mPosition.y = 0;

      // Update position of collider
      mCollider.circle().center(mPosition);
   }
}
