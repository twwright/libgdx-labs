package com.fatcat.gdxlabs.ws3.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.fatcat.gdxlabs.ws3.MazeGame;
import com.fatcat.gdxlabs.ws3.WhackCatGame;
import com.fatcat.gdxlabs.ws3.WhackCatGame2;
import com.fatcat.gdxlabs.ws3.WhackCatGame3;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 1200;
		config.height = 800;
		new LwjglApplication(new MazeGame(), config);
	}
}
