package com.fatcat.gdxlabs.ws3;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.ArrayList;
import java.util.List;

public class WhackCatGame2 extends ApplicationAdapter implements InputProcessor
{
	private List<Sprite> mTargets;
	private List<Float> mTargetLifespans;
	private Texture mAtlas;
	private TextureRegion mRegionCat;

	private float mMinSpawnTime = 0.5f;
	private float mMaxSpawnTime = 1.5f;
	private float mSpawnTime;
	private float mLifespan = 1f;

	private int mScore;
	private int mMissed;

	private Sound mSpawnSound;
	private Sound mHitSound;
	private Sound mMissedSound;
	private Music mMusic;

	private SpriteBatch mBatch;
	private BitmapFont mFont;

	@Override
	public void create ()
	{
		Gdx.input.setInputProcessor(this);
		mBatch = new SpriteBatch();
		mFont = new BitmapFont();
		mAtlas = new Texture("atlas.png");
		mRegionCat = new TextureRegion(mAtlas, 0, 0, 128, 128);
		mTargets = new ArrayList<Sprite>();
		mTargetLifespans = new ArrayList<Float>();
		mSpawnSound = Gdx.audio.newSound(Gdx.files.internal("spawn.wav"));
		mHitSound = Gdx.audio.newSound(Gdx.files.internal("hit.wav"));
		mMissedSound = Gdx.audio.newSound(Gdx.files.internal("miss.wav"));
		mMusic = Gdx.audio.newMusic(Gdx.files.internal("music2.mp3"));
		mMusic.setLooping(true);

		initialiseGame();
	}

	private void initialiseGame()
	{
		mTargets.clear();
		mTargetLifespans.clear();
		mScore = 0;
		mMissed = 0;
		mSpawnTime = mMinSpawnTime + ((float)Math.random() * mMaxSpawnTime - mMinSpawnTime);

		if(mMusic.isPlaying())
			mMusic.stop();
		mMusic.play();
	}

	@Override
	public void render()
	{
		// INPUT
		// handled separately in input processor (this class - methods below)

		// UPDATE
		float delta = Gdx.graphics.getDeltaTime();
		// Handle spawning a new target
		mSpawnTime -= delta;
		if(mSpawnTime <= 0)
		{
			// Play spawn sound
			float pitch = -0.1f + ((float)Math.random() * 0.2f);
			mSpawnSound.play(1f, 1f + pitch, 1f);
			// Randomise new target position
			int x = (int)(Math.random() * (Gdx.graphics.getWidth() - mRegionCat.getRegionWidth()));
			int y = (int)(Math.random() * (Gdx.graphics.getHeight() - mRegionCat.getRegionHeight()));
			// Create new target and add new lifespan value
			Sprite newTarget = new Sprite(mRegionCat);
			newTarget.setPosition(x, y);
			mTargets.add(newTarget);
			mTargetLifespans.add(mLifespan);
			// Randomise the spawn timer
			mSpawnTime = mMinSpawnTime + ((float)Math.random() * mMaxSpawnTime - mMinSpawnTime);
		}

		// Handle lifespans of existing targets (Iterate backwards so iteration not disturbed if we remove an element)
		for(int i = mTargets.size()-1; i >= 0; --i)
		{
			// Update lifespan
			float lifespan = mTargetLifespans.remove(i) - delta;
			if(lifespan <= 0)
			{
				// Play missed sound
				mMissedSound.play();
				// Remove the target as its lifespan has expired
				mTargets.remove(i);
				++mMissed;
			}
			else
			{
				// Return new lifespan value back into list
				mTargetLifespans.add(i, lifespan);
			}
		}

		// DRAW
		Gdx.gl.glClearColor(0.25f, 0.25f, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		mBatch.begin();
		for(Sprite target : mTargets)
		{
			target.draw(mBatch);
		}
		mFont.draw(mBatch, "Hit: " + mScore + " Missed: " + mMissed, 100, 100);
		mBatch.end();
	}

	@Override
	public void dispose()
	{
		mAtlas.dispose();
		mSpawnSound.dispose();
		mHitSound.dispose();
		mMissedSound.dispose();
		mMusic.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		// Check for game restart
		if(keycode == Input.Keys.SPACE)
		{
			// Restart game
			initialiseGame();
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		screenY = Gdx.graphics.getHeight() - screenY; // DIFFERENT COORDINATE SYSTEM
		for(int i = mTargets.size()-1; i >= 0; --i)
		{
			Sprite target = mTargets.get(i);
			int x = (int)target.getX(), y = (int)target.getY(), width = (int)target.getWidth(), height = (int)target.getHeight();
			// Check intersection with click
			if(screenX > x && screenX < x + width && screenY > y && screenY < y + height)
			{
				// Play hit sound
				mHitSound.play();
				// Update score
				++mScore;
				// Remove target
				mTargets.remove(i);
				mTargetLifespans.remove(i);
			}
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
