package com.fatcat.gdxlabs.ws3;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

/**
 * Created by Tom on 19/01/2015.
 */
public class MazeGenerator
{
   private static final float UP = 0f;
   private static final float DOWN = 1f;
   private static final float LEFT = 2f;
   private static final float RIGHT = 3f;

   public static boolean[][] generate(int screenWidth, int screenHeight, int cellSize)
   {
      int width = screenWidth / cellSize;
      if(width % 2 == 0)
         ++width;
      int height = screenHeight / cellSize;
      if(height % 2 == 0)
         ++height;
      boolean[][] maze = new boolean[width][height];
      ArrayList<Vector3> primList = new ArrayList<Vector3>();
      // Add starting walls
      maze[1][1] = true;
      primList.add(new Vector3(1, 1, UP));
      primList.add(new Vector3(1, 1, RIGHT));
      while(!primList.isEmpty())
      {
         // Determine the cell to check
         int rand = (int)(Math.random() * primList.size());
         Vector3 vec = primList.remove(rand);
         int checkX = (int)vec.x;
         if(vec.z == LEFT)
            checkX -= 2;
         else if(vec.z == RIGHT)
            checkX += 2;
         int checkY = (int)vec.y;
         if(vec.z == UP)
            checkY += 2;
         else if(vec.z == DOWN)
            checkY -= 2;

         // First ensure coordinates are within the maze, then check if this position is wall or passage
         if(checkX > 0 && checkX < width-1 && checkY > 0 && checkY < height-1) // is a wall
         {
            if(!maze[checkX][checkY]) {
               // Set the cell in the middle to a passage
               maze[((int) vec.x + checkX) / 2][((int) vec.y + checkY) / 2] = true;
               maze[checkX][checkY] = true;

               // Add new walls to list
               if(vec.z != UP)
                  primList.add(new Vector3(checkX, checkY, DOWN));
               if(vec.z != DOWN)
                  primList.add(new Vector3(checkX, checkY, UP));
               if(vec.z != LEFT)
                  primList.add(new Vector3(checkX, checkY, RIGHT));
               if(vec.z != RIGHT)
                  primList.add(new Vector3(checkX, checkY, LEFT));
            }
         }
      }
      return maze;
   }

   public static int getEndX(boolean[][] maze)
   {
      return (int)getEnd(maze).x;
   }

   public static int getEndY(boolean[][] maze)
   {
      return (int)getEnd(maze).y;
   }

   public static Vector2 getEnd(boolean[][] maze)
   {
      int width = maze.length;
      int height = maze[0].length;
      int x = width-1;
      int y = height-1;
      boolean moveX = true;
      while(x > 0 && y > 0 && !maze[x][y])
      {
         if(moveX)
            --x;
         else
            --y;
         moveX = !moveX;
      }
      return new Vector2(x, y);
   }
}
