package com.fatcat.gdxlabs.ws3;

import com.badlogic.gdx.*;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.io.FileNotFoundException;

public class MazeGame extends ApplicationAdapter
{
	public static final int MOVE_UP = 0;
	public static final int MOVE_LEFT = 1;
	public static final int MOVE_RIGHT = 2;
	public static final int MOVE_DOWN = 3;

	private boolean[][] mMaze;

	private Texture mAtlas;
	private TextureRegion mRegionUp;
	private TextureRegion mRegionDown;
	private TextureRegion mRegionLeft;
	private TextureRegion mRegionRight;
	private TextureRegion mRegionWall;
	private TextureRegion mRegionEnd;

	private int mSteps;
	private int mPlayerX;
	private int mPlayerY;
	private TextureRegion mPlayer;
	private int mEndX;
	private int mEndY;

	private Sound mWinSound;
	private Sound mStepSound;
	private Sound mWallSound;
	private Music mMusic;

	private SpriteBatch mBatch;
	private BitmapFont mFont;
	private KeyBindings mBindings;

	@Override
	public void create ()
	{
		mBatch = new SpriteBatch();
		mFont = new BitmapFont();
		mFont.setColor(Color.BLACK);

		// Load graphics assets
		mAtlas = new Texture("atlas2.png");
		mRegionUp = new TextureRegion(mAtlas, 0, 0, 64, 64);
		mRegionDown = new TextureRegion(mAtlas, 192, 0, 64, 64);
		mRegionLeft = new TextureRegion(mAtlas, 64, 0, 64, 64);
		mRegionRight = new TextureRegion(mAtlas, 128, 0, 64, 64);
		mRegionWall = new TextureRegion(mAtlas, 0, 64, 64, 64);
		mRegionEnd = new TextureRegion(mAtlas, 64, 64, 64, 64);

		// Load audio assets
		mWinSound = Gdx.audio.newSound(Gdx.files.internal("win.wav"));
		mWallSound = Gdx.audio.newSound(Gdx.files.internal("hit.wav"));
		mStepSound = Gdx.audio.newSound(Gdx.files.internal("miss.wav"));
		mMusic = Gdx.audio.newMusic(Gdx.files.internal("music2.mp3"));
		mMusic.setLooping(true);

		// Load key bindings
		try {
			mBindings = KeyBindings.loadFromFile("keys2.txt");
		} catch (FileNotFoundException e) {
			// Error loading bindings, set some defaults!
			mBindings = new KeyBindings();
			mBindings.setBinding("Left", Input.Keys.LEFT);
			mBindings.setBinding("Right", Input.Keys.RIGHT);
			mBindings.setBinding("Down", Input.Keys.DOWN);
			mBindings.setBinding("Up", Input.Keys.UP);
			mBindings.setBinding("Restart", Input.Keys.SPACE);
		}

		initialiseGame();
	}

	private void initialiseGame()
	{
		mPlayer = mRegionDown;
		mPlayerX = 1;
		mPlayerY = 1;
		mMaze = MazeGenerator.generate(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), mRegionWall.getRegionWidth());
		Vector2 end = MazeGenerator.getEnd(mMaze);
		mEndX = (int)end.x;
		mEndY = (int)end.y;
		if(mMusic.isPlaying())
			mMusic.stop();
		mMusic.play();
	}

	@Override
	public void render()
	{
		// INPUT
		int move = -1;
		// Check keyboard inputs
		if(mBindings.isActionJustPressed("Left"))
			move = MOVE_LEFT;
		else if(mBindings.isActionJustPressed("Right"))
			move = MOVE_RIGHT;
		else if(mBindings.isActionJustPressed("Down"))
			move = MOVE_DOWN;
		else if(mBindings.isActionJustPressed("Up"))
			move = MOVE_UP;

		// Check for game restart
		if(mBindings.isActionJustPressed("Restart"))
		{
			// Restart game
			initialiseGame();
		}

		// UPDATE
		// Handle moving the player
		int deltaX = 0;
		int deltaY = 0;
		if(move == MOVE_UP) {
			deltaY = 1;
			mPlayer = mRegionUp;
		}
		else if(move == MOVE_DOWN) {
			deltaY = -1;
			mPlayer = mRegionDown;
		}
		else if(move == MOVE_LEFT) {
			deltaX = -1;
			mPlayer = mRegionLeft;
		}
		else if(move == MOVE_RIGHT) {
			deltaX = 1;
			mPlayer = mRegionRight;
		}

		if(deltaX != 0 || deltaY != 0)
		{
			// Check if new position is not a wall (true)
			if(mMaze[mPlayerX + deltaX][mPlayerY + deltaY])
			{

				// Move player
				mPlayerX += deltaX;
				mPlayerY += deltaY;
				// Play step sound
				if(mPlayerX == mEndX && mPlayerY == mEndY)
					mWinSound.play();
				else
					mStepSound.play();
				++mSteps;
			}
			else
			{
				// Play wall sound
				mWallSound.play();
			}
		}

		// DRAW
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		mBatch.begin();
		for (int x = 0; x < mMaze.length; x++)
		{
			for (int y = 0; y < mMaze[x].length; y++)
			{
				if(!mMaze[x][y])
				{
					mBatch.draw(mRegionWall, x * mRegionWall.getRegionWidth(), y * mRegionWall.getRegionHeight());
				}
			}
		}
		mBatch.draw(mRegionEnd, mEndX * mRegionEnd.getRegionWidth(), mEndY * mRegionEnd.getRegionHeight());
		mBatch.draw(mPlayer, mPlayerX * mPlayer.getRegionWidth(), mPlayerY * mPlayer.getRegionHeight());
		BitmapFont.TextBounds bounds = mFont.getBounds(mSteps + " Steps");
		mFont.draw(mBatch, mSteps + " Steps", (mPlayerX+0.5f) * mPlayer.getRegionHeight() - (bounds.width / 2), mPlayerY * mPlayer.getRegionHeight());
		mBatch.end();
	}

	@Override
	public void dispose()
	{
		mAtlas.dispose();
		mStepSound.dispose();
		mWallSound.dispose();
		mWinSound.dispose();
		mMusic.dispose();
	}
}
