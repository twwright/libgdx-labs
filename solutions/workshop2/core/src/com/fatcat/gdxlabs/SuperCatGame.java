package com.fatcat.gdxlabs;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class SuperCatGame extends ApplicationAdapter
{
	private Texture mAtlas;

	private SpriteBatch batch;
	private Sprite moveCat1;
	private boolean movingUp = true;
	private Sprite moveCat2;
	private boolean movingRight = true;

	private Sprite spinCat1;
	private Sprite spinCat2;

	private float height;
	private float width;

	private int moveSpeed = 100; // pixels per second
	private int spinSpeed = 90; // degrees per second
	
	@Override
	public void create()
	{
		batch = new SpriteBatch();
		height = Gdx.graphics.getHeight();
		width = Gdx.graphics.getWidth();

		mAtlas = new Texture(Gdx.files.internal("atlas.png"));
		TextureRegion regionCat = new TextureRegion(mAtlas, 0, 0, 128, 128);

		moveCat1 = new Sprite(regionCat);
		moveCat1.setPosition(50, 50);

		moveCat2 = new Sprite(regionCat);
		moveCat2.setPosition(50, height - 50 - moveCat2.getHeight());

		spinCat1 = new Sprite(regionCat);
		spinCat1.setPosition(100, 100);

		spinCat2 = new Sprite(regionCat);
		spinCat2.setPosition(width - 100 - spinCat2.getWidth(), 100);
	}

	@Override
	public void render()
	{
		// INPUT - no inputs necessary

		// UPDATE
		update(Gdx.graphics.getDeltaTime());

		// DRAW
		draw();
	}

	@Override
	public void dispose()
	{
		mAtlas.dispose();
	}

	private void update(float deltaTime)
	{
		if(moveCat1.getY() + moveCat1.getHeight() >= height)
			movingUp = false;
		else if(moveCat1.getY() <= 0)
			movingUp = true;

		if(moveCat2.getX() + moveCat2.getWidth() >= width)
			movingRight = false;
		else if(moveCat2.getX() <= 0)
			movingRight = true;

		float move1 = ((movingUp) ? moveSpeed : -moveSpeed) * deltaTime;
		float move2 = ((movingRight) ? moveSpeed : -moveSpeed) * deltaTime;

		moveCat1.setPosition(moveCat1.getX(), moveCat1.getY() + move1);
		moveCat2.setPosition(moveCat2.getX() + move2, moveCat2.getY());

		float rotation1 = spinCat1.getRotation() + (spinSpeed * deltaTime);
		if(rotation1 >= 360)
			rotation1 -= 360;
		spinCat1.setRotation(rotation1);

		float rotation2 = spinCat2.getRotation() + (-spinSpeed * deltaTime);
		if(rotation2 <= -360)
			rotation2 += 360;
		spinCat2.setRotation(rotation2);
	}

	private void draw()
	{
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		moveCat1.draw(batch);
		moveCat2.draw(batch);
		spinCat1.draw(batch);
		spinCat2.draw(batch);
		batch.end();
	}
}
