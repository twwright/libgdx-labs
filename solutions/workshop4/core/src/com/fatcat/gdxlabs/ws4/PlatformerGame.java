package com.fatcat.gdxlabs.ws4;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tom on 2/02/2015.
 */
public class PlatformerGame extends ApplicationAdapter
{
   public static final float MAX_VELOCITY = 600;
   public static final float GROUND_Y = 70;
   public static final float LEFT_X = 95;
   public static final float RIGHT_X = 645;

   private Camera mCamera;
   private SpriteBatch mBatch;
   private Sprite mPlayer;
   private Texture mAtlas;

   private TextureRegion mRegionPlayer;
   private TextureRegion mRegionPlayerJump;
   private TextureRegion mRegionPlatform;

   private Vector2 mPosition;
   private Vector2 mJumpVelocity = new Vector2(0, 400);
   private float mMoveVelocity = 250;
   private Vector2 mGravity = new Vector2(0, -600);
   private Vector2 mVelocity = new Vector2(0, 0);


   @Override
   public void create()
   {
      mCamera = new OrthographicCamera(800, 480);
      mCamera.position.y = Gdx.graphics.getHeight()/2;

      mBatch = new SpriteBatch();
      mAtlas = new Texture(Gdx.files.internal("atlas2.png"));
      mRegionPlayer = new TextureRegion(mAtlas, 0, 0, 66, 92);
      mRegionPlayerJump = new TextureRegion(mAtlas, 0, 92, 66, 92);
      mRegionPlatform = new TextureRegion(mAtlas, 100, 0, 800, 512);

      initialiseGame();
   }

   @Override
   public void render()
   {
      Gdx.gl.glClearColor(0.9f, 0.8f, 0.9f, 1);
      Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

      updatePlayer(Gdx.graphics.getDeltaTime());

      mCamera.update();

      mBatch.setProjectionMatrix(mCamera.combined);
      mBatch.begin();
      mBatch.draw(mRegionPlatform, 0, 0);
      mPlayer.draw(mBatch);
      mBatch.end();
   }

   @Override
   public void dispose()
   {
      // Dispose of our atlas to free the memory
      mAtlas.dispose();
   }

   private void initialiseGame()
   {
      // Create or recreate the player and set the position vector
      mPlayer = new Sprite(mRegionPlayer);
      mPosition = new Vector2(100, 300);
      mPlayer.setPosition(mPosition.x, mPosition.y);
   }

   public void updatePlayer(float delta)
   {
      // Check for jump (button pressed and on ground)
      if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE) && mPosition.y == GROUND_Y)
      {
         // Add our jump velocity to the player velocity
         mVelocity.add(mJumpVelocity);
      }

      // Apply movement left and right
      if(mPosition.y == GROUND_Y)
      {
         // If we are on the ground, stop moving if no key is pressed
         mVelocity.x = 0;
         // If left or right is pressed, set horizontal velocity to a constant speed
         if(Gdx.input.isKeyPressed(Input.Keys.LEFT))
            mVelocity.x = -mMoveVelocity;
         if(Gdx.input.isKeyPressed(Input.Keys.RIGHT))
            mVelocity.x = mMoveVelocity;
      }
      else
      {
         // If we are not on the ground, don't reset horizontal velocity
         // If left or right is pressed, add the move speed to the existing velocity
         if(Gdx.input.isKeyPressed(Input.Keys.LEFT))
            mVelocity.add(new Vector2(-mMoveVelocity, 0).scl(delta));
         if(Gdx.input.isKeyPressed(Input.Keys.RIGHT))
            mVelocity.add(new Vector2(mMoveVelocity, 0).scl(delta));
      }

      // If travelling up or down, set to jumping sprite
      if(mVelocity.y != 0)
         mPlayer.setRegion(mRegionPlayerJump);
      else // Otherwise we must be on the ground, set the standing sprite
         mPlayer.setRegion(mRegionPlayer);

      // Apply (scaled) gravity to player velocity
      Vector2 scaledGravity = mGravity.cpy().scl(delta);
      mVelocity.add(scaledGravity);

      // Clamp velocity to maximums (terminal velocity)
      if(mVelocity.x > MAX_VELOCITY)
         mVelocity.x = MAX_VELOCITY;
      if(mVelocity.y < -MAX_VELOCITY)
         mVelocity.y = -MAX_VELOCITY;
      if(mVelocity.y > MAX_VELOCITY)
         mVelocity.y = MAX_VELOCITY;
      if(mVelocity.y < -MAX_VELOCITY)
         mVelocity.y = -MAX_VELOCITY;

      // Apply player velocity to player position (scaled by delta)
      mPosition.add(mVelocity.cpy().scl(delta));

      // Check for collision with floor or walls
      // If passing through a boundary, reset to the boundary and stop velocity
      if(mPosition.y < GROUND_Y)
      {
         mPosition.y = GROUND_Y;
         mVelocity.y = 0;
      }
      if(mPosition.x < LEFT_X)
      {
         mPosition.x = LEFT_X;
         mVelocity.x = 0;
      }
      if(mPosition.x > RIGHT_X)
      {
         mPosition.x = RIGHT_X;
         mVelocity.x = 0;
      }

      // If moving left, flip the sprite
      if(mVelocity.x < 0)
         mPlayer.setScale(-1, 1);
      else if(mVelocity.x > 0) // If moving right, don't flip the sprite
         mPlayer.setScale(1, 1);



      // Update sprite position to match position vector
      mPlayer.setPosition(mPosition.x, mPosition.y);
      // Set camera horizontal position
      mCamera.position.x = mPlayer.getX() + mPlayer.getWidth()/2;
   }
}
