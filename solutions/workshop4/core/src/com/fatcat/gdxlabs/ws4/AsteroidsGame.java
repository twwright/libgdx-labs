package com.fatcat.gdxlabs.ws4;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.List;

public class AsteroidsGame extends ApplicationAdapter
{
	public static final int NUM_ASTEROIDS = 15;
	public static final int MIN_POSITION = -500;
	public static final int MAX_POSITION = 500;

	private Texture mAtlas;
	private TextureRegion mRegionStation;
	private TextureRegion mRegionShip;
	private TextureRegion mRegionShip2;
	private TextureRegion mRegionAsteroid;

	private Camera mCamera;
	private SpriteBatch mBatch;

	private Sprite mStation;
	private float mStationRotation = 90f;

	private Ship mShip;

	private List<Asteroid> mAsteroids;
	
	@Override
	public void create () {
		mBatch = new SpriteBatch();
		mCamera = new OrthographicCamera(800, 480); // Create camera size with a common size
		mAtlas = new Texture(Gdx.files.internal("atlas.png"));

		mRegionStation = new TextureRegion(mAtlas, 0, 0, 91, 91);
		mRegionAsteroid = new TextureRegion(mAtlas, 91, 0, 65, 64);
		mRegionShip = new TextureRegion(mAtlas, 0, 128, 85, 103);
		mRegionShip2 = new TextureRegion(mAtlas, 85, 128, 85, 103);

		initialiseGame();
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0.1f, 0f, 0.5f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		float delta = Gdx.graphics.getDeltaTime();

		float mouseX = -1, mouseY = -1;
		boolean clicked = false;
		if (Gdx.input.isTouched())
		{
			clicked = true;
			mouseX = Gdx.input.getX();
			mouseY = Gdx.input.getY();
		}
		// Convert screen coordinates to world coordinates
		Vector3 destinationCoords = mCamera.unproject(new Vector3(mouseX, mouseY, 0));

		updateStation(delta);
		mShip.update(delta, clicked ? new Vector2(destinationCoords.x, destinationCoords.y) : null);
		updateAsteroids(delta);

		// If ship isn't moving, see if we can pick up or drop off an asteroid
		if(!mShip.isMoving())
		{
			if(mShip.isCarryingAsteroid())
				tryDropOff();
			else
				tryPickUp();
		}

		mCamera.position.set(mShip.getX() + mShip.getWidth()/2, mShip.getY() + mShip.getHeight()/2, 0);
		mCamera.update();

		mBatch.setProjectionMatrix(mCamera.combined);
		mBatch.begin();
		mStation.draw(mBatch);
		mShip.draw(mBatch);
		for(Asteroid asteroid : mAsteroids)
			asteroid.draw(mBatch);
		mBatch.end();
	}

	@Override
	public void dispose()
	{
		mAtlas.dispose();
	}

	public void initialiseGame()
	{
		mStation = new Sprite(mRegionStation);
		mStation.setPosition(100, 100);
		mShip = new Ship(mRegionShip, mRegionShip2, 180, 45, 1f, Interpolation.exp5);
		mShip.setPosition(200, 100);
		mShip.setCarryingAsteroid(true);
		generateAsteroids();
	}

	public void generateAsteroids()
	{
		mAsteroids = new ArrayList<Asteroid>();
		for(int i = 0; i < NUM_ASTEROIDS; ++i)
		{
			// Generate position
			float posX = MIN_POSITION + (float)Math.random() * (MAX_POSITION - MIN_POSITION);
			float posY = MIN_POSITION + (float)Math.random() * (MAX_POSITION - MIN_POSITION);
			// Generate rotation
			float rotation = -90 + (float)Math.random() * 180;
			// Add asteroid
			Asteroid asteroid = new Asteroid(mRegionAsteroid, rotation);
			asteroid.setPosition(posX, posY);
			mAsteroids.add(asteroid);
		}
	}

	private void updateStation(float delta)
	{
		float rotation = mStation.getRotation() + mStationRotation * delta;
		if(rotation > 360)
			rotation -= 360;
		mStation.setRotation(rotation);
	}

	private void updateAsteroids(float delta)
	{
		for(Asteroid asteroid : mAsteroids)
		{
			asteroid.update(delta);
		}
	}

	private void tryDropOff()
	{
		Vector2 stationPos = new Vector2(mStation.getX() + mStation.getWidth()/2, mStation.getY() + mStation.getHeight()/2);
		Vector2 shipPos = new Vector2(mShip.getX() + mShip.getWidth()/2, mShip.getY() + mStation.getHeight() / 2);
		// Check direction to station
		Vector2 toStation = stationPos.cpy().sub(shipPos);
		Vector2 facing = mShip.getForwardVector();
		float dot = toStation.dot(facing);
		if(dot > 0) // if direction to station is in front of ship
		{
			// Check distance to station
			float stationDistance = stationPos.dst(shipPos);
			if(stationDistance < 100) // if close enough
			{
				mShip.setCarryingAsteroid(false);
				System.out.println("Dropped Off Asteroid!");
			}
		}
	}

	private void tryPickUp()
	{
		// Check each asteroid to see if we can pick it up
		for(int i = 0; i < mAsteroids.size(); ++i)
		{
			// Create vectors to use in checking
			Asteroid asteroid = mAsteroids.get(i);
			Vector2 asteroidPos = new Vector2(asteroid.getX() + asteroid.getWidth()/2, asteroid.getY() + asteroid.getHeight()/2);
			Vector2 shipPos = new Vector2(mShip.getX() + mShip.getWidth()/2, mShip.getY() + mStation.getHeight() / 2);
			// Check angle to this asteroid is within the pickup angle
			// Need to normalise to check the angle!
			Vector2 toAsteroid = asteroidPos.cpy().sub(shipPos).nor();
			Vector2 facing = mShip.getForwardVector();
			float angle = (float)Math.toDegrees(Math.acos(toAsteroid.dot(facing)));
			if(Math.abs(angle) < mShip.getPickupAngle()) // if direction to asteroid is in pickup angle
			{
				// Check distance to asteroid
				float asteroidDistance = asteroidPos.dst(shipPos);
				if(asteroidDistance < 100) // if close enough
				{
					// Modify ship to be carrying asteroid
					mShip.setCarryingAsteroid(true);
					System.out.println("Pickup Up Asteroid!");
					// Delete this asteroid from the list of asteroids
					mAsteroids.remove(i);
					// Break from the loop - we only want to pick up one asteroid!
					break;
				}
			}
		}
	}
}
