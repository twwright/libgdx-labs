package com.fatcat.gdxlabs.ws4;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import java.util.List;

/**
 * Created by Tom on 2/02/2015.
 */
public class Ship extends Sprite
{
   private TextureRegion mEmptyTex;
   private TextureRegion mFullTex;
   private boolean mIsCarrying = false;
   private float mRotationSpeed;
   private float mPickupAngle;
   private Vector2 mOrigin;
   private Vector2 mDestination;
   private Vector2 mDestinationVector;
   private float mLerpTime;
   private float mLerpTimer;
   private Interpolation mInterpolation;
   public Ship(TextureRegion emptyTexture, TextureRegion fullTexture, float rotationSpeed, float pickupAngle, float lerpTime, Interpolation interpolation)
   {
      super(emptyTexture);
      mEmptyTex = emptyTexture;
      mFullTex = fullTexture;
      mRotationSpeed = rotationSpeed;
      mPickupAngle = pickupAngle;
      mLerpTime = lerpTime;
      mInterpolation = interpolation;
   }

   public void setCarryingAsteroid(boolean carrying)
   {
      setRegion(carrying ? mFullTex : mEmptyTex);
      mIsCarrying = carrying;
   }

   public boolean isCarryingAsteroid()
   {
      return mIsCarrying;
   }

   public boolean isMoving()
   {
      return mDestination != null;
   }

   public float getPickupAngle()
   {
      return mPickupAngle;
   }

   public Vector2 getForwardVector()
   {
      // Construct vector for current rotation using sprite rotation
      float x = (float)Math.cos(Math.toRadians(getRotation()));
      float y = (float)Math.sin(Math.toRadians(getRotation()));
      return new Vector2(x, y);
   }

   public void update(float delta, Vector2 destination)
   {
      if(mDestinationVector != null) // Currently rotating, proceed with that
      {
         Vector2 facing = getForwardVector();
         // Acos of the dot product gives the angle between the ship rotation and the destination in radians
         float differenceAngle = (float)Math.acos(mDestinationVector.dot(facing));
         // Convert to degrees
         differenceAngle = (float)Math.toDegrees(differenceAngle);
         if(Math.abs(differenceAngle) < 5f) // Error margin - stop rotating when less than five degree error
         {
            mDestinationVector = null;
         }
         else
         {
            float rotation = getRotation() + mRotationSpeed * delta;
            if(rotation > 360)
               rotation -= 360;
            setRotation(rotation);
         }
      }
      else if(mDestination != null) // Currently moving, proceed with that
      {
         mLerpTimer -= delta;
         if(mLerpTimer <= 0)
         {
            // Lerp completed
            setPosition(mDestination.x - getWidth()/2, mDestination.y - getHeight()/2);
            mDestination = null;
            mOrigin = null;
         }
         else
         {
            float posX = mInterpolation.apply(mOrigin.x, mDestination.x, 1f - mLerpTimer / mLerpTime);
            float posY = mInterpolation.apply(mOrigin.y, mDestination.y, 1f - mLerpTimer / mLerpTime);
            setPosition(posX - getWidth() / 2, posY - getHeight() / 2);
         }
      }
      else if(destination != null) // Not moving, respond to click
      {
         // Set our interpolation variables
         mOrigin = new Vector2(getX() + getWidth()/2, getY() + getHeight()/2);
         mDestination = new Vector2(destination.x, destination.y);
         // Set the vector that points to the destination from the ship center
         // Copy the destination vector first
         // Important that we remember to normalise!
         mDestinationVector = mDestination.cpy().sub(mOrigin).nor();
         mLerpTimer = mLerpTime;
      }
   }
}
