package com.fatcat.gdxlabs.ws4;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by Tom on 4/02/2015.
 */
public class {


   public static void main(String[] args)
   {
      method();
      method2();
   }
   public static void method() {
      Vector2 pos = new Vector2(3, 2);

      float posX = pos.x;
      float posY = pos.y;

      System.out.println(posX + posY);


      Vector2 firstBounce = new Vector2(0, 2);

      Vector2 secondBounce = firstBounce.cpy().scl(0.7f); // (0, 1.4)
      Vector2 thirdBounce = secondBounce.cpy().scl(0.7f); // (0, 1.0)
      Vector2 fourthBounce = thirdBounce.cpy().scl(0.7f); // (0, 0.7)

      System.out.println(fourthBounce);


      Vector2 knightPos = new Vector2(4, 1);
      Vector2 knightMove = new Vector2(-1, 2);

      knightPos.add(knightMove); // (3, 3)

      Vector2 pawnPos = new Vector2(2, 2);
      Vector2 pawnMove = new Vector2(0, 1);

      pawnPos.add(pawnMove); // (2, 3)


      Vector2 minerPos = new Vector2(3, 1);
      Vector2 gemPos = new Vector2(2, 1);

      // Copy the gem position so we don't overwrite!
      Vector2 minerToGemOffset = gemPos.cpy().sub(minerPos);

      System.out.println(minerToGemOffset);

      float speed = 2;
      Vector2 direction = new Vector2(1, 1); // Not unit vector!

      // magnitude = ~2.8 <-- wrong!
      Vector2 badVelocity = direction.cpy().scl(speed);

      direction.nor(); // Normalise the vector - length 1

      // magnitude = 2 <-- good!
      Vector2 correctVelocity = direction.cpy().scl(speed);

      System.out.println(correctVelocity);

   }

   public static void method2()
   {
      Vector2 minerPos = new Vector2(4, 1);
      Vector2 gemPos = new Vector2(1, 2);

      // Miner facing up and can see 45 degrees either side
      Vector2 minerDirection = new Vector2(0, 1);
      float minerViewAngle = 45;

      // Copy the gem position so we don't overwrite!
      Vector2 minerToGem = gemPos.cpy().sub(minerPos);
      minerToGem.nor(); // Normalise to use as direction

      float dot = minerDirection.dot(minerToGem); // 0.32

      float angleRadians = (float)Math.acos(dot); // 1.25 (radians!)
      float angle = (float)Math.toDegrees(angleRadians); // 71.6

      if(angle < minerViewAngle)
         System.out.println("I see gems!");
      else
         System.out.println("Hmmm, no gems yet...");

      Vector2 dir = new Vector2(-2, 1);

      Vector2 perpLeft = new Vector2(-dir.y, dir.x); // (-1, -2)
      Vector2 perpRight = new Vector2(dir.y, -dir.x); // (1, 2)

      System.out.println(perpLeft);
      System.out.println(perpRight);

      Texture regionCat = new Texture("fgd.png");
      Sprite cat = new Sprite(regionCat);

      Vector2 startPos = new Vector2(100, 200);
      Vector2 endPos = new Vector2(300, 100);

      float timer = 0f;
      float duration = 1f; // Interpolate over 1 second

      float delta = Gdx.graphics.getDeltaTime();
      timer += delta;

      // If lerp complete
      if(timer > duration)
      {
         cat.setPosition(endPos.x, endPos.y);
         // Do something now that move is finished
      }
      else
      {
         float fraction = timer / duration;
         // Interpolate using lerp - could use Interpolation class
         Vector2 lerpPos = startPos.cpy().lerp(endPos, fraction);

         cat.setPosition(lerpPos.x, lerpPos.y);
      }

      // Halfway linearly between the two positions
      Vector2 lerpPos = startPos.cpy().lerp(endPos, 0.5f);

      // Halfway between using the bounce easing function
      float bounceX = Interpolation.bounce.apply(startPos.x, endPos.x, 0.5f);
      float bounceY = Interpolation.bounce.apply(startPos.y, endPos.y, 0.5f);
      Vector2 bouncePos = new Vector2(bounceX, bounceY);

      System.out.println(lerpPos);
      System.out.println(bouncePos);

      float start = 0;
      float end = 10;

      // apply(start value, end value, interpolation value 0 -> 1)
      float elastic = Interpolation.elastic.apply(start, end, 0.2f);
      float circle = Interpolation.circle.apply(start, end, 0.5f);
      float bounce = Interpolation.bounce.apply(start, end, 0.7f);

      System.out.println(elastic + " " + circle + " " + bounce);
   }

   public void interpolateCat()
   {

   }

   public void method3()
   {
      Camera camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
      SpriteBatch batch = new SpriteBatch();

      // Modify camera position in some way (tracking player, cursor, etc.
      camera.position.x += 5;

      camera.update(); // Recalculate projection matrix

      batch.setProjectionMatrix(camera.combined); // Apply matrix to rendering
      batch.begin();
      // Rendering positions passed through projection matrix
      batch.end();

      float screenX = Gdx.input.getX(), screenY = Gdx.input.getY();

      // Necessary to use Vector3 - just use 0 as z value
      Vector3 screenCoords = new Vector3(screenX, screenY, 0);

      // Woohoo - world coordinates!
      Vector3 worldCoords = camera.unproject(screenCoords.cpy());

      System.out.println(worldCoords);
   }
}
