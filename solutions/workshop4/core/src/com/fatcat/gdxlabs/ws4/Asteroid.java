package com.fatcat.gdxlabs.ws4;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Tom on 2/02/2015.
 */
public class Asteroid extends Sprite
{
   private boolean mSpinningRight;
   private float mRotationSpeed;

   public Asteroid(TextureRegion tex, float rotationSpeed)
   {
      super(tex);
      mRotationSpeed = rotationSpeed;
   }

   public void update(float delta)
   {
      float rotation = getRotation() + mRotationSpeed * delta;
      if(rotation > 360)
         rotation -= 360;
      if(rotation < 0)
         rotation += 360;
      setRotation(rotation);
   }
}
