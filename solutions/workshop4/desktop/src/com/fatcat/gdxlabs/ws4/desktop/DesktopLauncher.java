package com.fatcat.gdxlabs.ws4.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.fatcat.gdxlabs.ws4.AsteroidsGame;
import com.fatcat.gdxlabs.ws4.PlatformerGame;
import com.fatcat.gdxlabs.ws4.Test;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 800;
		config.height = 480;
		new LwjglApplication(new Test(), config);
	}
}
