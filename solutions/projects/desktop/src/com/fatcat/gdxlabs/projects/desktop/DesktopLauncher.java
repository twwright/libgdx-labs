package com.fatcat.gdxlabs.projects.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.fatcat.gdxlabs.projects.asteroids.AsteroidsGame;
import com.fatcat.gdxlabs.projects.checkers.CheckersGame;
import com.fatcat.gdxlabs.projects.plane.PlaneGame;
import com.fatcat.gdxlabs.projects.platformer.PlatformerGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new CheckersGame(), config);
	}
}
