package com.fatcat.gdxlabs.projects.asteroids;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.GameObject;
import com.fatcat.gdxlabs.projects.game.GameWorld;
import com.fatcat.gdxlabs.projects.states.State;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tom on 23/02/2015.
 */
public class AsteroidsState extends State
{
   private Color mBackground;
   private List<GameObject> mStoredGameObjects = new LinkedList<GameObject>();

   public AsteroidsState(String name, Color background)
   {
      super(name);
      mBackground = background;
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // Background colour
      Gdx.gl.glClearColor(mBackground.r, mBackground.g, mBackground.b, mBackground.a);
      Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

      // Check for pause!
      AsteroidsWorld asteroidsWorld = (AsteroidsWorld)world;
      if(asteroidsWorld.getKeyBindings().isActionJustPressed(AsteroidsGame.KEY_PAUSE))
      {
         world.getStateManager().setNewState(AsteroidsGame.STATE_PAUSE);
      }
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      // Draw lives?
   }

   @Override
   public void enter(State oldState, GameWorld world)
   {
      if(oldState.getName().equals(AsteroidsGame.STATE_PAUSE))
      {
         for(GameObject storedObj : mStoredGameObjects)
            world.add(storedObj);
      }
      else
      {
         // Randomise stars
         for(int i = 0; i < AsteroidsGame.NUM_STARS; ++i)
         {
            Vector2 position = new Vector2((float)Math.random() * world.size().x, (float)Math.random() * world.size().y);
            world.add(new Star(Resources.tStar, position));
         }

         // Randomise asteroids
         float padding = AsteroidsGame.SPAWN_BORDER;
         float width = world.size().x, height = world.size().y;
         float[] xPositions = new float[] { padding, width - padding, width - padding, padding};
         float[] yPositions = new float[] { height - padding, height - padding, padding, padding};
         for(int i = 0; i < AsteroidsGame.NUM_ASTEROIDS; ++i)
         {
            // Randomly place asteroid on left or bottom border
            Vector2 position = new Vector2(xPositions[i], yPositions[i]);
            // Randomise velocity vector
            float rot = (float)(Math.random() * Math.PI * 2);
            Vector2 velocity = new Vector2((float)Math.cos(rot), (float)Math.sin(rot)).scl(50);
            // Create asteroid
            TextureRegion[] asteroidTextures = new TextureRegion[] { Resources.tAsteroidL, Resources.tAsteroidM, Resources.tAsteroidS };
            world.add(new Asteroid(position, asteroidTextures, Resources.sBounce, 0, asteroidTextures[0].getRegionWidth()/2, velocity));
         }

         // Place ship
         world.add(new Ship(world.size().cpy().scl(0.5f), Resources.tShip, Resources.tLaser, Resources.sShoot, Resources.sExplode, Resources.tShip.getRegionWidth()/2, 0.75f, 200f, 180f, 0f));
      }
      mStoredGameObjects.clear();
   }

   @Override
   public void exit(State newState, GameWorld world)
   {
      // Play click
      if(Resources.sClick != null)
         Resources.sClick.play();
      // Store them all!
      mStoredGameObjects.addAll(world.getGameObjects());
      world.removeAll();
   }
}
