package com.fatcat.gdxlabs.projects.checkers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.fatcat.gdxlabs.projects.game.GameWorld;
import com.fatcat.gdxlabs.projects.states.Button;
import com.fatcat.gdxlabs.projects.states.ExitButton;
import com.fatcat.gdxlabs.projects.states.State;
import com.fatcat.gdxlabs.projects.states.StateChangeButton;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tom on 28/02/2015.
 */
public class CheckersState extends State
{
   private static final Vector2[] PLAYER_ONE_MOVES = { new Vector2(1, 1), new Vector2(-1, 1)};
   private static final Vector2[] PLAYER_TWO_MOVES = { new Vector2(1, -1), new Vector2(-1, -1)};
   private static final Vector2[] ALL_MOVES = { new Vector2(1, 1), new Vector2(-1, 1), new Vector2(1, -1), new Vector2(-1, -1)};

   public static int PLAYER_ONE = 0;
   public static int PLAYER_TWO = 1;


   private Vector2 mSelected;
   private boolean mHasJumped = false;
   private List<Vector2> mLegalMoves;

   private Piece[][] mPieces;
   private Board mBoard;

   private CheckersWorld mWorld;

   public CheckersState(String name)
   {
      super(name);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // Handle clicks and touches
      if(Gdx.input.justTouched())
      {
         // Retrieve touch coordinates and unproject with camera
         Vector3 screenCoords = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
         world.getCamera().unproject(screenCoords);

         Vector2 worldCoords = new Vector2(screenCoords.x, screenCoords.y);

         // Check if transformed screen coordinates are inside the board
         Rectangle rect = new Rectangle(mBoard.position().x, mBoard.position().y, mBoard.size().x * mBoard.cellSize().x, mBoard.size().y * mBoard.cellSize().y);
         if(rect.contains(worldCoords))
         {
            // Represent click relative to board
            worldCoords.sub(mBoard.position());
            int indexX = (int)(worldCoords.x / mBoard.cellSize().x);
            int indexY = (int)(worldCoords.y / mBoard.cellSize().y);
            // Run logic for click on that cell
            onClick(new Vector2(indexX, indexY));
         }
      }
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      // Draw move indicator on legal moves
      if(mSelected != null)
      {
         for(Vector2 move : mLegalMoves)
         {
            Vector2 drawPos = mBoard.getCellCenter(move);
            drawPos.sub(Resources.tMove.getRegionWidth() / 2, Resources.tMove.getRegionHeight() / 2);
            batch.draw(Resources.tMove, drawPos.x, drawPos.y);
         }
      }
   }

   @Override
   public void enter(State oldState, GameWorld world)
   {
      // Reset some variables
      mWorld = (CheckersWorld)world;
      mWorld.setPlayer(PLAYER_ONE);
      mSelected = null;
      mLegalMoves = null;

      // Recreate the board
      mPieces = new Piece[6][6];
      mBoard = new Board(new Vector2(15,15), new Vector2(6, 6), Resources.tSquareLight, Resources.tSquareDark);
      world.add(mBoard);
      // Recreate the pieces
      for(int y = 0; y < mPieces[0].length; ++y)
      {
         for(int x = 0; x < mPieces.length; ++x)
         {
            Vector2 cellPos = new Vector2(x, y);
            Piece piece = null;
            // Create light pieces on first two rows, each second square
            if(y <= 1 && ((x % 2 == 0 && y % 2 == 0) || (x % 2 == 1 && y % 2 == 1)))
            {
               piece = new Piece(mBoard.getCellCenter(cellPos), PLAYER_ONE, Resources.tPieceLight, Resources.tKingLight, 180f, 1.2f);
            }
            else if(y >= 4 && ((x % 2 == 0 && y % 2 == 0) || (x % 2 == 1 && y % 2 == 1))) // Create dark pieces
            {
               piece = new Piece(mBoard.getCellCenter(cellPos), PLAYER_TWO, Resources.tPieceDark, Resources.tKingDark, 180f, 1.2f);
            }

            if(piece != null)
            {
               // Add piece
               mPieces[x][y] = piece;
               world.add(piece);
            }
         }
      }

      // Create buttons
      Button exitButton = new ExitButton(new Vector2(640, 60), Resources.tQuit, Resources.tQuitSelected, Resources.sMove);
      world.add(exitButton);
      Button restartButton = new StateChangeButton(new Vector2(640, world.size().y-60), Resources.tRestart, Resources.tRestartSelected, CheckersGame.STATE_GAME, Resources.sMove);
      world.add(restartButton);

      // Create cursor
      world.add(new Cursor(Vector2.Zero, Resources.tCursor, Resources.tCursorLight, Resources.tCursorDark));
   }

   @Override
   public void exit(State newState, GameWorld world)
   {
      world.removeAll();
   }

   private void onClick(Vector2 cell)
   {
      // Attempted move or deselect
      if(mSelected != null)
      {
         // If clicked on selected piece and it is allowed, deselect
         if(mSelected.equals(cell) && mLegalMoves.contains(cell))
         {
            mPieces[(int)mSelected.x][(int)mSelected.y].setSelected(false);
            mSelected = null;
         }
         else if(mLegalMoves.contains(cell))
         {
            // Legal move, so do move
            moveSelected(cell);
         }
         else if(Resources.sBadClick != null)
         {
            // Illegal move
            Resources.sBadClick.play();
         }
      }
      else // Selecting a piece
      {
         // Does the cell contain a piece?
         Piece clicked = mPieces[(int)cell.x][(int)cell.y];
         if(clicked != null)
         {
            // Did the player click on their piece?
            if(clicked.getPlayer() == mWorld.getPlayer())
            {
               // Select that piece
               clicked.setSelected(true);
               mSelected = cell;
               // Calculate legal moves!
               findLegalMoves();
            }
            else if(Resources.sBadClick != null)
            {
               // Play incorrect sound
               Resources.sBadClick.play();
            }
         }
      }
   }

   private boolean findLegalMoves()
   {
      // Retrieve the currently selected piece
      Piece selectedPiece = mPieces[(int)mSelected.x][(int)mSelected.y];

      // Determine possible moves based on player and if the piece is a king
      Vector2[] moves;
      if(selectedPiece.isKing())
         moves = ALL_MOVES;
      else if(selectedPiece.getPlayer() == PLAYER_ONE)
         moves = PLAYER_ONE_MOVES;
      else
         moves = PLAYER_TWO_MOVES;


      List<Vector2> moveList = new LinkedList<Vector2>();
      List<Vector2> jumpList = new LinkedList<Vector2>();
      // Find which moves are legal
      for(Vector2 move : moves)
      {
         Vector2 movePos = mSelected.cpy().add(move);
         Vector2 jumpPos = movePos.cpy().add(move);
         boolean moveOnBoard = movePos.x >= 0 && movePos.x < mBoard.size().x && movePos.y >= 0 && movePos.y < mBoard.size().y;
         boolean jumpOnBoard = jumpPos.x >= 0 && jumpPos.x < mBoard.size().x && jumpPos.y >= 0 && jumpPos.y < mBoard.size().y;

         Piece movePiece = moveOnBoard ? mPieces[(int)movePos.x][(int)movePos.y] : null;
         Piece jumpPiece = jumpOnBoard ? mPieces[(int)jumpPos.x][(int)jumpPos.y] : null;
         if(moveOnBoard && movePiece == null)
            moveList.add(movePos);
         else if(jumpOnBoard && movePiece.getPlayer() != selectedPiece.getPlayer() && jumpPiece == null)
            jumpList.add(jumpPos);
      }

      // Only allow regular moves if no jumps can be made!
      if(jumpList.isEmpty())
         mLegalMoves = moveList;
      else
         mLegalMoves = jumpList;

      // If we haven't moved yet, we can deselect!
      if(!mHasJumped)
         mLegalMoves.add(mSelected);

      // Return true if a jump can be made, false otherwise!
      return !jumpList.isEmpty();
   }

   private void moveSelected(Vector2 newPos)
   {
      // Move the selected piece and update things
      Piece selectedPiece = mPieces[(int)mSelected.x][(int)mSelected.y];
      selectedPiece.position(mBoard.getCellCenter(newPos));
      mPieces[(int)newPos.x][(int)newPos.y] = mPieces[(int)mSelected.x][(int)mSelected.y];
      mPieces[(int)mSelected.x][(int)mSelected.y] = null;

      // Play move sound
      if(Resources.sMove != null)
         Resources.sMove.play();

      // Remove jumped piece if we jumped
      if(Math.abs(mSelected.x - newPos.x) == 2f) // Jumping something
      {
         // Record that we just made a jump
         mHasJumped = true;

         // Play take sound
         if(Resources.sTake != null)
            Resources.sTake.play();

         // Remove the jumped piece!
         Vector2 jumped = mSelected.cpy().lerp(newPos, 0.5f);
         Piece jumpedPiece = mPieces[(int)jumped.x][(int)jumped.y];
         mPieces[(int)jumped.x][(int)jumped.y] = null;
         mWorld.updateRemove(jumpedPiece);

         // Check if this player has pieces left!
         if(pieceCount(jumpedPiece.getPlayer()) == 0)
         {
            // Player that just moved won!
            mWorld.getStateManager().setNewState(CheckersGame.STATE_OVER);
            // When state changes, world still has active player set
            return;
         }
      }

      // Update selected cell
      mSelected = newPos;

      // Check for king upgrade!
      if(!selectedPiece.isKing() &&
            ((selectedPiece.getPlayer() == PLAYER_ONE && mSelected.y == mBoard.size().y-1) ||
                  (selectedPiece.getPlayer() == PLAYER_TWO && mSelected.y == 0)))
      {
         if(Resources.sKing != null)
            Resources.sKing.play();
         selectedPiece.setKing(true);
      }

      // Recalculate legal moves
      boolean canJump = findLegalMoves();

      // If we cannot jump or we haven't already made a move, deselect the piece
      // and it is the next players turn!
      if(!canJump || !mHasJumped)
      {
         // Deselect
         selectedPiece.setSelected(false);
         mSelected = null;
         // Change player
         mWorld.setPlayer((mWorld.getPlayer() + 1) % 2);
         // Remove has jumped flag
         mHasJumped = false;
      }
   }

   // Counts the number of remaining pieces for the given player
   private int pieceCount(int player)
   {
      int count = 0;
      for(int y = 0; y < mBoard.size().y; ++y)
      {
         for(int x = 0; x < mBoard.size().x; ++x)
         {
            if(mPieces[x][y] != null && mPieces[x][y].getPlayer() == player)
               ++count;
         }
      }
      return count;
   }
}
