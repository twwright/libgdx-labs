package com.fatcat.gdxlabs.projects.asteroids;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.*;

import java.util.List;

/**
 * Created by Tom on 15/02/2015.
 */
public class Asteroid extends GameObject implements IDrawable, IUpdateable, ICollidable, IShootable
{
   private Collider mCollider;
   private Sprite mSprite;

   private TextureRegion[] mTextures;
   private int mIndex = 0;

   private Sound mBounceSound;

   private Vector2 mVelocity;
   private float mRotationSpeed;

   public Asteroid(Vector2 position, TextureRegion[] rockTextures, Sound bounceSound, int index, float radius, Vector2 velocity)
   {
      super(position);
      mVelocity = velocity;
      mRotationSpeed = velocity.len();
      mTextures = rockTextures;
      mIndex = index;
      mBounceSound = bounceSound;
      mSprite = new Sprite(rockTextures[index]);
      mCollider = new Collider(position.cpy(), radius);
   }

   @Override
   public Collider getCollider()
   {
      return mCollider;
   }

   @Override
   public boolean isSolid()
   {
      return true;
   }

   @Override
   public boolean isStatic()
   {
      return false;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      if(other.isSolid())
      {
         // Play bounce sound
         if(mBounceSound != null)
         {
            float pitchMod = -0.1f + (float)Math.random() * 0.2f;
            mBounceSound.play(1f, 1f + pitchMod, 0f);
         }
         // Circle collision, so velocity direction is to be away from collision
         Vector2 dir = mPosition.cpy().sub(((GameObject) other).position()).nor();
         // Scale direction to magnitude of current velocity times the multiplier
         dir.scl(mVelocity.len());
         mVelocity = dir;

         // Reverse rotation
         mRotationSpeed = -mRotationSpeed;
      }
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // Modify sprite rotation
      mSprite.rotate(mRotationSpeed * delta);

      // Modify position
      mPosition.add(mVelocity.cpy().scl(delta));

      // Wrap around vertically
      if(mPosition.y - mSprite.getHeight()/2 > world.size().y)
         mPosition.y = 0 - mSprite.getHeight()/2;
      else if(mPosition.y + mSprite.getHeight()/2 < 0)
         mPosition.y = world.size().y + mSprite.getHeight()/2;

      // Wrap around horizontally
      if(mPosition.x - mSprite.getWidth()/2 > world.size().x)
         mPosition.x = 0 - mSprite.getWidth()/2;
      else if(mPosition.x + mSprite.getWidth()/2 < 0)
         mPosition.x = world.size().x + mSprite.getWidth()/2;

      // Update collider position
      mCollider.circle().center(mPosition);
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);

      mSprite.draw(batch);
   }

   @Override
   public void onShot(GameWorld world)
   {
      // Deactivate this asteroid
      this.setActive(false);

      // Create smaller asteroids if possible
      if(mIndex < mTextures.length-1)
      {
         Asteroid left = new Asteroid(mPosition, mTextures, mBounceSound, mIndex + 1, mTextures[mIndex + 1].getRegionWidth() / 2, new Vector2(mVelocity.y, -mVelocity.x).scl(2f));
         Asteroid right = new Asteroid(mPosition, mTextures, mBounceSound, mIndex + 1, mTextures[mIndex + 1].getRegionWidth() / 2, new Vector2(-mVelocity.y, mVelocity.x).scl(2f));
         world.updateAdd(left);
         world.updateAdd(right);
      }
      else // Count the number of active asteroids remaining, return to menu if zero
      {
         // Count the number of asteroids currently in the world
         List<GameObject> objects = world.getGameObjects();
         int asteroidCount = 0;
         for(GameObject obj : objects)
         {
            if(obj instanceof Asteroid && obj.isActive())
               ++asteroidCount;
         }

         // Finish game if count is zero!
         if(asteroidCount == 0)
            world.getStateManager().setNewState(AsteroidsGame.STATE_MENU);
      }

      // Remove self
      world.updateRemove(this);
   }
}
