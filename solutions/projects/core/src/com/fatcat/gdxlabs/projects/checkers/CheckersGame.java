package com.fatcat.gdxlabs.projects.checkers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.fatcat.gdxlabs.projects.game.Collision;
import com.fatcat.gdxlabs.projects.game.CompositionStateGame;

import java.awt.*;

/**
 * Created by Tom on 27/02/2015.
 */
public class CheckersGame extends CompositionStateGame
{
   public static final String STATE_GAME = "game";
   public static final String STATE_OVER = "gameOver";

   private Texture mAtlas;

   @Override
   public void create()
   {
      super.create();

      // Not actually using collision in this game! But need to instantiate to avoid null ptr exception
      mCollision = new Collision(Collision.CIRCLE_MODE);

      // Load assets
      mAtlas = new Texture(Gdx.files.internal("checkersAtlas.png"));
      Resources.tSquareLight = new TextureRegion(mAtlas, 0, 0, 75, 75);
      Resources.tSquareDark = new TextureRegion(mAtlas, 75, 0, 75, 75);
      Resources.tPieceLight = new TextureRegion(mAtlas, 0, 75, 75, 75);
      Resources.tPieceDark = new TextureRegion(mAtlas, 75, 75, 75, 75);
      Resources.tKingLight = new TextureRegion(mAtlas, 0, 150, 75, 75);
      Resources.tKingDark = new TextureRegion(mAtlas, 75, 150, 75, 75);
      Resources.tCursorLight = new TextureRegion(mAtlas, 150, 0, 75, 75);
      Resources.tCursorDark = new TextureRegion(mAtlas, 225, 0, 75, 75);
      Resources.tCursor = new TextureRegion(mAtlas, 150, 75, 75, 75);
      Resources.tMove = new TextureRegion(mAtlas, 225, 75, 75, 75);

      Resources.tRestart = new TextureRegion(mAtlas, 0, 225, 225, 75);
      Resources.tRestartSelected = new TextureRegion(mAtlas, 225, 225, 225, 75);
      Resources.tQuit = new TextureRegion(mAtlas, 0, 300, 225, 75);
      Resources.tQuitSelected = new TextureRegion(mAtlas, 225, 300, 225, 75);

      Resources.sKing = Gdx.audio.newSound(Gdx.files.internal("select.wav"));
      Resources.sMove = Gdx.audio.newSound(Gdx.files.internal("click.mp3"));
      Resources.sTake = Gdx.audio.newSound(Gdx.files.internal("laser.wav"));
      Resources.sBadClick = Gdx.audio.newSound(Gdx.files.internal("bounce.wav"));
      Resources.sWin = Gdx.audio.newSound(Gdx.files.internal("score.wav"));

      // Reinitialise world
      mWorld = new CheckersWorld(mWorld);
      // Create states
      GameOverState gameOverState = new GameOverState(STATE_OVER, Resources.tKingLight, Resources.tKingDark);
      mStateManager.registerState(gameOverState);

      CheckersState gameState = new CheckersState(STATE_GAME);
      mStateManager.registerState(gameState);

      initialiseGame();
   }

   @Override
   public void initialiseGame()
   {
      mStateManager.setNewState(STATE_GAME);
   }

   @Override
   public void dispose()
   {
      super.dispose();
      mAtlas.dispose();
      Resources.sMove.dispose();
      Resources.sWin.dispose();
      Resources.sTake.dispose();
      Resources.sBadClick.dispose();
      Resources.sKing.dispose();
   }
}
