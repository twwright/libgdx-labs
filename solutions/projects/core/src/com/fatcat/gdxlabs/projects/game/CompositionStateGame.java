package com.fatcat.gdxlabs.projects.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.states.StateManager;

import java.util.List;

/**
 * Created by Tom on 12/02/2015.
 */
public abstract class CompositionStateGame extends ApplicationAdapter
{
   public static boolean colliderDrawEnabled = false;
   protected SpriteBatch mBatch;
   protected ShapeRenderer mRenderer; // For drawing primitives - used to debug the colliders - see below
   protected StateManager mStateManager;
   protected Camera mCamera;
   protected GameWorld mWorld;
   protected Collision mCollision;

   @Override
   public void create ()
   {
      mBatch = new SpriteBatch();
      mRenderer = new ShapeRenderer();
      mStateManager = new StateManager();
      mCamera = new OrthographicCamera(800, 480);
      mCamera.position.set(mCamera.viewportWidth/2, mCamera.viewportHeight/2, 0);
      mWorld = new GameWorld(new Vector2(mCamera.viewportWidth, mCamera.viewportHeight), mCamera, mStateManager);
   }

   @Override
   public void render ()
   {
      Gdx.gl.glClearColor(1f, 1f, 1f, 1);
      Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

      if(Gdx.input.isKeyJustPressed(Input.Keys.F1) || !mWorld.isRunning())
         initialiseGame(); // Reset

      // Add and remove objects designated in update
      mWorld.performUpdateAddRemove();

      // Allow the state manager to change states if necessary
      mStateManager.changeState(mWorld);

      // Update the current state
      if(mStateManager.getCurrentState() != null)
         mStateManager.getCurrentState().update(mWorld, Gdx.graphics.getDeltaTime());

      // Update the game world and camera
      update();
      mCamera.update();

      // Collision
      mCollision.collision(mWorld.getCollideables(), mWorld);

      // Draw the game world
      draw();
      // Draw the current state
      if(mStateManager.getCurrentState() != null)
      {
         mBatch.setProjectionMatrix(mCamera.combined);
         mBatch.begin();
         mStateManager.getCurrentState().draw(mBatch);
         mBatch.end();
      }
   }

   public void update()
   {
      List<IUpdateable> updateables = mWorld.getUpdateables();
      for(IUpdateable updateable : updateables)
      {
         if(((GameObject)updateable).isActive())
            updateable.update(mWorld, Gdx.graphics.getDeltaTime());
      }
   }

   public void draw()
   {
      List<IDrawable> drawables = mWorld.getDrawables();
      mBatch.setProjectionMatrix(mCamera.combined);
      mBatch.begin();
      for(IDrawable drawable : drawables)
      {
         if(((GameObject)drawable).isActive())
            drawable.draw(mBatch);
      }
      mBatch.end();

      // Debug drawing of colliders
      if(colliderDrawEnabled)
      {
         mRenderer.setProjectionMatrix(mCamera.combined);
         mRenderer.begin(ShapeRenderer.ShapeType.Line);
         for (ICollidable collideable : mWorld.getCollideables()) {
            if (((GameObject) collideable).isActive())
               mCollision.drawCollider(mRenderer, collideable);
         }
         mRenderer.end();
      }
   }

   public abstract void initialiseGame();
}
