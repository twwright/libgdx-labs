package com.fatcat.gdxlabs.projects.checkers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.GameWorld;
import com.fatcat.gdxlabs.projects.states.Button;
import com.fatcat.gdxlabs.projects.states.State;
import com.fatcat.gdxlabs.projects.states.StateChangeButton;

/**
 * Created by Tom on 1/03/2015.
 */
public class GameOverState extends State
{
   private Button mButton;
   private Cursor mCursor;

   private TextureRegion mPlayerOne;
   private TextureRegion mPlayerTwo;
   private Sprite mSprite;

   public GameOverState(String name, TextureRegion playerOne, TextureRegion playerTwo)
   {
      super(name);
      mPlayerOne = playerOne;
      mPlayerTwo = playerTwo;
      mButton = new StateChangeButton(Vector2.Zero, Resources.tRestart, Resources.tRestartSelected, CheckersGame.STATE_GAME, Resources.sMove);
      mCursor = new Cursor(Vector2.Zero, Resources.tCursor, Resources.tCursorLight, Resources.tCursorDark);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      mSprite.rotate(90f * delta);
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      mSprite.draw(batch);
   }

   @Override
   public void enter(State oldState, GameWorld world)
   {
      // Play win sound
      if(Resources.sKing != null)
         Resources.sKing.play();
      // Create winning player sprite
      CheckersWorld cWorld = (CheckersWorld)world;
      TextureRegion winner = cWorld.getPlayer() == CheckersState.PLAYER_ONE ? mPlayerOne : mPlayerTwo;
      mSprite = new Sprite(winner);
      mSprite.setPosition(world.size().x/2 - mSprite.getWidth()/2, world.size().y/2 - mSprite.getHeight()/2);
      // Add button and cursor to world
      mButton.position().set(world.size().x/2, 60);
      world.add(mButton);
      world.add(mCursor);
   }

   @Override
   public void exit(State newState, GameWorld world)
   {
      world.remove(mButton);
      world.remove(mCursor);
   }
}
