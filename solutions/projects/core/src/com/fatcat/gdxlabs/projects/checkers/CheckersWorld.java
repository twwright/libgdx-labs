package com.fatcat.gdxlabs.projects.checkers;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.GameWorld;
import com.fatcat.gdxlabs.projects.states.StateManager;

/**
 * Created by Tom on 1/03/2015.
 */
public class CheckersWorld extends GameWorld
{
   private int mCurrentPlayer = -1;

   public CheckersWorld(Vector2 size, Camera camera, StateManager manager)
   {
      super(size, camera, manager);
   }

   public CheckersWorld(GameWorld world)
   {
      this(world.size(), world.getCamera(), world.getStateManager());
   }

   public int getPlayer()
   {
      return mCurrentPlayer;
   }

   public void setPlayer(int player)
   {
      mCurrentPlayer = player;
   }
}
