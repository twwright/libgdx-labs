package com.fatcat.gdxlabs.projects.asteroids;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.GameWorld;
import com.fatcat.gdxlabs.projects.game.KeyBindings;
import com.fatcat.gdxlabs.projects.states.StateManager;

/**
 * Created by Tom on 21/02/2015.
 */
public class AsteroidsWorld extends GameWorld
{
   private KeyBindings mBindings;

   public AsteroidsWorld(Vector2 size, Camera camera, StateManager manager, KeyBindings bindings)
   {
      super(size, camera, manager);
      mBindings = bindings;
   }

   public AsteroidsWorld(GameWorld gameWorld, KeyBindings bindings)
   {
      this(gameWorld.size(), gameWorld.getCamera(), gameWorld.getStateManager(), bindings);
   }

   public KeyBindings getKeyBindings()
   {
      return mBindings;
   }
}
