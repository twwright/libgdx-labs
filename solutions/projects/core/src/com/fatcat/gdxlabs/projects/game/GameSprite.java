package com.fatcat.gdxlabs.projects.game;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Tom on 23/02/2015.
 */
public class GameSprite extends GameObject implements IDrawable
{
   private Sprite mSprite;

   public GameSprite(Vector2 position, TextureRegion texture)
   {
      super(position);
      mSprite = new Sprite(texture);
   }
   @Override
   public void draw(SpriteBatch batch)
   {
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      mSprite.draw(batch);
   }
}
