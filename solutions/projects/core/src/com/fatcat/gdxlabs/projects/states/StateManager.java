package com.fatcat.gdxlabs.projects.states;

import com.fatcat.gdxlabs.projects.game.GameWorld;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tom on 15/02/2015.
 */
public class StateManager
{
   private List<State> mStates = new ArrayList<State>();
   private State mCurrentState;
   private State mNewState;

   public void registerState(State state)
   {
      if(!mStates.contains(state))
         mStates.add(state);
   }

   public State getCurrentState()
   {
      return mCurrentState;
   }

   public void setNewState(String stateName)
   {
      State newState = findState(stateName);
      if(newState != null)
         mNewState = newState;
   }

   public void changeState(GameWorld world)
   {
      if(mNewState != null)
      {
         if(mCurrentState != null)
            mCurrentState.exit(mNewState, world);
         mNewState.enter(mCurrentState, world);
         mCurrentState = mNewState;
         mNewState = null;
      }
   }

   private State findState(String stateName)
   {
      for(State state : mStates)
      {
         if(state.getName().equals(stateName))
            return state;
      }
      return null;
   }
}
