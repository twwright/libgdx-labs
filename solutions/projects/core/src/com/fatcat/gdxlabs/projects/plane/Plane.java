package com.fatcat.gdxlabs.projects.plane;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.*;

/**
 * Created by Tom on 24/02/2015.
 */
public class Plane extends GameObject implements IUpdateable, IDrawable, ICollidable
{
   private Collider mCollider;
   private Sprite mSprite;
   private TextureRegion mGameOver;
   private Vector2 mGameOverPos;

   private float mRotation;

   private float mVelocity;
   private float mTapImpulse;
   private float mGravity;

   private boolean mDestroyed = false;
   private float mDestroyedRotationSpeed;

   public Plane(Vector2 position, TextureRegion texture, TextureRegion gameOverTexture, float gravity, float tapImpulse, float destroyedRotationSpeed, Vector2 gameOverPos)
   {
      super(position);
      mSprite = new Sprite(texture);
      mCollider = new Collider(mSprite.getBoundingRectangle());
      mGravity = gravity;
      mTapImpulse = tapImpulse;
      mDestroyedRotationSpeed = destroyedRotationSpeed;
      mGameOver = gameOverTexture;
      mGameOverPos = gameOverPos;
   }

   @Override
   public Collider getCollider()
   {
      return mCollider;
   }

   @Override
   public boolean isSolid()
   {
      return false;
   }

   @Override
   public boolean isStatic()
   {
      return false;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      if(other.isSolid() && !mDestroyed)
      {
         // Play lose sound
         if(Resources.sLose != null)
            Resources.sLose.play();
         mDestroyed = true;
         mVelocity = 0;
      }
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      // Update sprite position and rotation, then draw
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      mSprite.setRotation(mRotation);
      mSprite.draw(batch);

      // Draw game over texture in center if destroyed
      if(mDestroyed)
      {
         batch.draw(mGameOver, mGameOverPos.x - mGameOver.getRegionWidth()/2, mGameOverPos.y - mGameOver.getRegionHeight()/2);
      }
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      if(mDestroyed)
         updateDestroyed(world, delta);
      else
         updateUndestroyed(world, delta);

      // Apply gravity to velocity
      mVelocity += mGravity * delta;

      // Apply velocity to position
      mPosition.y += mVelocity * delta;

      // Update collider center
      mCollider.rect().setCenter(mPosition.x, mPosition.y);
   }

   public boolean isDestroyed()
   {
      return mDestroyed;
   }

   private void updateDestroyed(GameWorld world, float delta)
   {
      // Destroyed plane spins out of control!
      mRotation += mDestroyedRotationSpeed * delta;

      // If destroyed plane below edge of screen, return to menu!
      if(mPosition.y + mSprite.getHeight()/2 < 0)
         world.getStateManager().setNewState(PlaneGame.STATE_MENU);
   }

   private void updateUndestroyed(GameWorld world, float delta)
   {
      // Destroy the plane if it goes below the ground or above the top of the screen
      if(mPosition.y < 0 || mPosition.y > world.size().y)
      {
         // Play lose sound
         if(Resources.sLose != null)
            Resources.sLose.play();
         mVelocity = 0;
         mDestroyed = true;
      }

      // Check for touch (or sneaky space bar) and apply jump
      if(Gdx.input.justTouched() || Gdx.input.isKeyJustPressed(Input.Keys.SPACE))
      {
         // Play jump sound
         if(Resources.sJump != null)
            Resources.sJump.play();

         // If velocity is less than zero, it becomes zero
         if(mVelocity < 0)
            mVelocity = 0;
         // Apply jump velocity
         mVelocity += mTapImpulse;
      }

      // Calculate rotation of plane based on velocity
      mRotation = -45 * (mVelocity / mGravity);
   }
}
