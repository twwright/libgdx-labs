package com.fatcat.gdxlabs.projects.platformer;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.GameWorld;
import com.fatcat.gdxlabs.projects.game.KeyBindings;
import com.fatcat.gdxlabs.projects.states.StateManager;

import java.util.List;

/**
 * Created by Tom on 25/02/2015.
 */
public class PlatformerWorld extends GameWorld
{
   private List<Level> mLevels;
   private int mCurrentLevelIndex = 0;

   private int mCoins = 0;

   private KeyBindings mBindings;

   public PlatformerWorld(Vector2 size, Camera camera, StateManager manager, List<Level> levels, KeyBindings bindings)
   {
      super(size, camera, manager);
      mLevels = levels;
      mBindings = bindings;
   }

   public PlatformerWorld(GameWorld gameWorld, List<Level> levels, KeyBindings bindings)
   {
      this(gameWorld.size(), gameWorld.getCamera(), gameWorld.getStateManager(), levels, bindings);
   }

   public Level getLevel()
   {
      return mLevels.get(mCurrentLevelIndex);
   }

   public int getNumLevels()
   {
      return mLevels.size();
   }

   public int getCurrentLevelIndex()
   {
      return mCurrentLevelIndex;
   }

   public void setCurrentLevelIndex(int index)
   {
      if(index >= 0 && index < mLevels.size())
         mCurrentLevelIndex = index;
   }

   public int getCoins()
   {
      return mCoins;
   }

   public void setCoins(int coins)
   {
      mCoins = coins;
   }

   public void addCoins(int coins)
   {
      mCoins += coins;
   }

   public KeyBindings getBindings()
   {
      return mBindings;
   }
}
