package com.fatcat.gdxlabs.projects.checkers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.fatcat.gdxlabs.projects.game.GameObject;
import com.fatcat.gdxlabs.projects.game.GameWorld;
import com.fatcat.gdxlabs.projects.game.IDrawable;
import com.fatcat.gdxlabs.projects.game.IUpdateable;

import java.util.List;

/**
 * Created by Tom on 28/02/2015.
 */
public class Board extends GameObject implements IDrawable
{
   private Vector2 mSize;
   private Vector2 mCellSize;
   private TextureRegion mSquareLight;
   private TextureRegion mSquareDark;


   public Board(Vector2 position, Vector2 size, TextureRegion squareLight, TextureRegion squareDark)
   {
      super(position);
      mSize = size;
      mCellSize = new Vector2(squareLight.getRegionWidth(), squareLight.getRegionHeight());
      mSquareDark = squareDark;
      mSquareLight = squareLight;
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      for(int y = 0; y < mSize.y; ++y)
      {
         for(int x = 0; x < mSize.x; ++x)
         {
            TextureRegion square = (x % 2 == 0 && y % 2 == 0) || (x % 2 == 1 && y % 2 == 1) ?  mSquareLight : mSquareDark;
            batch.draw(square, mPosition.x + x * square.getRegionWidth(), mPosition.y + y * square.getRegionHeight());
         }
      }
   }

   public Vector2 size()
   {
      return mSize;
   }

   public Vector2 cellSize()
   {
      return mCellSize;
   }

   public Vector2 getCellCenter(Vector2 cell)
   {
      Vector2 vec = cell.cpy();
      vec.scl(mSquareLight.getRegionWidth(), mSquareLight.getRegionHeight());
      vec.add(mSquareLight.getRegionWidth()/2, mSquareLight.getRegionHeight()/2);
      vec.add(mPosition);
      return vec;
   }
}
