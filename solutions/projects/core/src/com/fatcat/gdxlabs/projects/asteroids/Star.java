package com.fatcat.gdxlabs.projects.asteroids;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.GameObject;
import com.fatcat.gdxlabs.projects.game.IDrawable;

/**
 * Created by Tom on 12/02/2015.
 */
public class Star extends GameObject implements IDrawable
{
   private Sprite mSprite;

   public Star(TextureRegion texture, Vector2 position)
   {
      super(position);
      mSprite = new Sprite(texture);
   }
   @Override
   public void draw(SpriteBatch batch)
   {
      mSprite.setPosition(mPosition.x, mPosition.y);
      mSprite.draw(batch);
   }
}
