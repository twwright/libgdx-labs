package com.fatcat.gdxlabs.projects.asteroids;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.*;

/**
 * Created by Tom on 21/02/2015.
 */
public class Laser extends GameObject implements IUpdateable, IDrawable, ICollidable
{
   private Sprite mSprite;
   private Collider mCollider;

   private Vector2 mVelocity;

   public Laser(Vector2 position, TextureRegion texture, Vector2 velocity, float rotation, float radius)
   {
      super(position);
      mVelocity = velocity;
      mSprite = new Sprite(texture);
      mSprite.setRotation(rotation);
      mCollider = new Collider(position.cpy(), radius);
   }

   @Override
   public Collider getCollider()
   {
      return mCollider;
   }

   @Override
   public boolean isSolid() {
      return false;
   }

   @Override
   public boolean isStatic() {
      return false;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      // Check if the other thing if shootable
      if(other instanceof IShootable)
      {
         ((IShootable)other).onShot(world);
         if(Resources.sHit != null)
            Resources.sHit.play();
         this.setActive(false);
         world.updateRemove(this);
      }
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      mSprite.draw(batch);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // Update position
      mPosition.add(mVelocity.cpy().scl(delta));

      // Wrap around vertically
      if(mPosition.y - mSprite.getHeight()/2 > world.size().y)
         mPosition.y = 0 - mSprite.getHeight()/2;
      else if(mPosition.y + mSprite.getHeight()/2 < 0)
         mPosition.y = world.size().y + mSprite.getHeight()/2;

      // Wrap around horizontally
      if(mPosition.x - mSprite.getWidth()/2 > world.size().x)
         mPosition.x = 0 - mSprite.getWidth()/2;
      else if(mPosition.x + mSprite.getWidth()/2 < 0)
         mPosition.x = world.size().x + mSprite.getWidth()/2;

      // Update collider
      mCollider.circle().center(mPosition);
   }
}
