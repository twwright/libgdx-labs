package com.fatcat.gdxlabs.projects.plane;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.*;

/**
 * Created by Tom on 24/02/2015.
 */
public class PointScore extends GameObject implements IUpdateable, ICollidable
{
   private Collider mCollider;
   private float mVelocity;

   public PointScore(Vector2 position, float velocity, float size)
   {
      super(position);
      mVelocity = velocity;
      mCollider = new Collider(new Rectangle(position.x - size/2, position.y - size/2, size, size));
   }


   @Override
   public Collider getCollider()
   {
      return mCollider;
   }

   @Override
   public boolean isSolid() {
      return false;
   }

   @Override
   public boolean isStatic() {
      return false;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      if(other instanceof Plane && !((Plane)other).isDestroyed())
      {
         // Play score sound
         if(Resources.sScore != null)
            Resources.sScore.play();
         // Update score
         PlaneWorld planeWorld = (PlaneWorld)world;
         planeWorld.setScore(planeWorld.getScore() + 1);
         // Deactive and remove
         setActive(false);
         world.updateRemove(this);
      }
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // Update position
      mPosition.x += mVelocity * delta;

      // Remove self if outside of game window
      if(mPosition.x < 0)
         world.updateRemove(this);

      // Update collider position
      mCollider.rect().setCenter(mPosition.x, mPosition.y);
   }
}
