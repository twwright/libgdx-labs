package com.fatcat.gdxlabs.projects.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Tom on 11/02/2015.
 */
public interface IDrawable
{
   public void draw(SpriteBatch batch);
}
