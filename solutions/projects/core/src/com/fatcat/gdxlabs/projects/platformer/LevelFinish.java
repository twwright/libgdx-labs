package com.fatcat.gdxlabs.projects.platformer;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.*;

/**
 * Created by Tom on 25/02/2015.
 */
public class LevelFinish extends GameObject implements IDrawable, IUpdateable, ICollidable
{
   private Collider mCollider;
   private Sprite mSprite;

   private TextureRegion mRegionOpen;
   private TextureRegion mRegionClosed;
   private TextureRegion[] mRegionDigits;

   private boolean mOpen = false;
   private int mRequiredCoins;
   private int mCollectedCoins;

   public LevelFinish(Vector2 position, TextureRegion textureOpen, TextureRegion textureClosed, TextureRegion[] textureDigits, int requiredCoins)
   {
      super(position);
      mRegionOpen = textureOpen;
      mRegionClosed = textureClosed;
      mRegionDigits = textureDigits;
      mSprite = new Sprite(mRegionClosed);
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      // Create a small collider in the middle of the door
      mCollider = new Collider(new Rectangle(mPosition.x, mPosition.y, 2, 2));
      mRequiredCoins = requiredCoins;
   }

   @Override
   public Collider getCollider() {
      return mCollider;
   }

   @Override
   public boolean isSolid()
   {
      return false;
   }

   @Override
   public boolean isStatic()
   {
      return true;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      if(mOpen && other instanceof Player && ((Player)other).isAlive())
      {
         // Play level finish sound
         if(Resources.sFinish != null)
            Resources.sFinish.play();

         // Change the level!
         PlatformerWorld pWorld = (PlatformerWorld)world;
         if(pWorld.getCurrentLevelIndex() < pWorld.getNumLevels()-1)
         {
            // Move to next level
            pWorld.setCurrentLevelIndex(pWorld.getCurrentLevelIndex()+1);
            pWorld.getStateManager().setNewState(PlatformerGame.STATE_GAME);
         }
         else
         {
            // Return to menu, game over, you win!
            pWorld.getStateManager().setNewState(PlatformerGame.STATE_MENU);
         }
      }
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // Check the number of coins collected
      mCollectedCoins = ((PlatformerWorld)world).getCoins();

      if(mCollectedCoins >= mRequiredCoins && !mOpen)
      {
         // Play level finish sound to let the player know the door opened
         if(Resources.sFinish != null)
            Resources.sFinish.play();

         mOpen = true;
         mSprite.setRegion(mRegionOpen);
      }
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      // Draw self
      mSprite.draw(batch);
      // Draw digit on top to represent number of coins required
      if(!mOpen)
      {
         TextureRegion digit = mRegionDigits[mRequiredCoins - mCollectedCoins];
         batch.draw(digit, mPosition.x - digit.getRegionWidth()/2, mPosition.y - digit.getRegionHeight()/2);
      }
   }
}
