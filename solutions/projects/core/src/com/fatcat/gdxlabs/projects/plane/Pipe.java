package com.fatcat.gdxlabs.projects.plane;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.*;

/**
 * Created by Tom on 24/02/2015.
 */
public class Pipe extends GameObject implements IUpdateable, IDrawable, ICollidable
{
   private Collider mCollider;
   private Sprite mSprite;

   private float mVelocity;

   public Pipe(Vector2 position, TextureRegion textureRegion, float velocity, boolean isFlipped)
   {
      super(position);
      mSprite = new Sprite(textureRegion);
      mSprite.setFlip(false, isFlipped);
      mCollider = new Collider(mSprite.getBoundingRectangle());
      mVelocity = velocity;
   }

   @Override
   public Collider getCollider()
   {
      return mCollider;
   }

   @Override
   public boolean isSolid()
   {
      return true;
   }

   @Override
   public boolean isStatic()
   {
      return false;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      // Nothing?
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      mSprite.draw(batch);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // Update position
      mPosition.x += mVelocity * delta;

      // Remove self if outside of screen area
      if(mPosition.x + mSprite.getWidth()/2 < 0)
      {
         setActive(false);
         world.updateRemove(this);
      }

      // Update collider position
      mCollider.rect().setCenter(mPosition.x, mPosition.y);
   }
}
