package com.fatcat.gdxlabs.projects.plane;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.GameSprite;
import com.fatcat.gdxlabs.projects.game.GameWorld;
import com.fatcat.gdxlabs.projects.states.State;

/**
 * Created by Tom on 24/02/2015.
 */
public class PlaneState extends State
{
   private TextureRegion[] mDigits;
   private Vector2 mScorePos;
   private int mScore;

   private float mMaxGapOffset;
   private float mGapHeight;
   private float mGapWidth;
   private float mSpeed;

   private float mDistanceTravelled = 0;

   public PlaneState(String name, TextureRegion[] digits, Vector2 scorePos, float speed, float gapHeight, float maxGapOffset, float gapWidth)
   {
      super(name);
      mDigits = digits;
      mScorePos = scorePos;
      mScore = 0;
      mSpeed = speed;
      mGapHeight = gapHeight;
      mGapWidth = gapWidth;
      mMaxGapOffset = maxGapOffset;
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // Possibly create pipes
      mDistanceTravelled += mSpeed * delta;
      if(mDistanceTravelled > mGapWidth)
      {
         createPipes(world);
         mDistanceTravelled = 0;
      }

      // Retrieve score for render step
      mScore = ((PlaneWorld)world).getScore();
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      // Render score as digits
      String score = String.valueOf(mScore);
      float padding = 10;
      Vector2 digitPos = mScorePos.cpy().add(padding, padding);
      for(int i = 0; i < score.length(); ++i)
      {
         int digit = Integer.parseInt(score.substring(i, i+1));
         TextureRegion texDigit = mDigits[digit];
         batch.draw(texDigit, digitPos.x, digitPos.y);
         digitPos.x += texDigit.getRegionWidth() + padding;
      }
   }

   @Override
   public void enter(State oldState, GameWorld world)
   {
      // Clear world
      world.removeAll();

      // Reset score and distance travelled
      ((PlaneWorld)world).setScore(0);
      mDistanceTravelled = 0;

      // Create background element
      world.add(new GameSprite(world.size().cpy().scl(0.5f), Resources.tBackground));

      // Create ground elements
      float gameWidth = world.size().x + Resources.tGround.getRegionWidth();
      float groundWidth = Resources.tGround.getRegionWidth();
      int numGround = (int)(gameWidth / groundWidth);
      for(int x = 0; x <= numGround; ++x)
      {
         world.add(new Ground(new Vector2(x * groundWidth, 0f), Resources.tGround, -mSpeed, (numGround+1) * groundWidth));
      }

      // Create plane
      world.add(new Plane(new Vector2(world.size().x / 2, world.size().y / 2), Resources.tPlane, Resources.tGameOver, -700f, 350f, 360f, new Vector2(world.size().x/2, world.size().y/2)));

      // Spawn first pipes
      createPipes(world);
   }

   @Override
   public void exit(State newState, GameWorld world)
   {
      // Clear all game objects, we are returning to the menu
      world.removeAll();
   }

   private void createPipes(GameWorld world)
   {
      // Calculate gap height
      float gapY = world.size().y/2;
      gapY += -mMaxGapOffset + Math.random() * mMaxGapOffset * 2;
      float gapX = world.size().x + Resources.tPipe.getRegionWidth()/2;

      // Add point score box
      world.add(new PointScore(new Vector2(gapX, gapY), -mSpeed, 50f));

      // Add pipes

      float bottomY = gapY - mGapHeight/2 - Resources.tPipe.getRegionHeight()/2;
      float topY = gapY + mGapHeight/2 + Resources.tPipe.getRegionHeight()/2;

      world.add(new Pipe(new Vector2(gapX, bottomY), Resources.tPipe, -mSpeed, false));
      world.add(new Pipe(new Vector2(gapX, topY), Resources.tPipe, -mSpeed, true));
   }
}
