package com.fatcat.gdxlabs.projects.platformer;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.*;

/**
 * Created by Tom on 21/02/2015.
 */
public class Enemy extends GameObject implements IUpdateable, ICollidable, IDrawable
{
   private Collider mCollider;
   private Sprite mSprite;
   private Vector2 mVelocity;
   private final Vector2 mGravity = new Vector2(0, -700f);

   public Enemy(TextureRegion texture, Vector2 position, Vector2 initialVelocity)
   {
      super(position);
      mSprite = new Sprite(texture);
      mCollider = new Collider(mSprite.getBoundingRectangle());
      mVelocity = initialVelocity;
   }

   @Override
   public Collider getCollider()
   {
      return mCollider;
   }

   @Override
   public boolean isSolid()
   {
      return true;
   }

   @Override
   public boolean isStatic()
   {
      return false;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      // Check if player
      if(other instanceof Player && ((Player)other).isAlive())
      {

         Player p = (Player)other;
         // Kill player
         p.kill();
      }
      else
      {
         // Determine direction of collision and set velocity in that axis to 0
         Vector2 otherPos = ((GameObject)other).position();
         Vector2 directionToOther = otherPos.cpy().sub(mPosition).nor();
         Vector2 up = new Vector2(0, 1);

         // How similar is the direction to up
         float dot = directionToOther.dot(up);

         // If dot is close to 0 (close to perpendicular - left or right)
         if(Math.abs(dot) < 0.25f)
            mVelocity.scl(-1, 1); // Change direction (reverse horizontal velocity)
         else // Collision is above or below
            mVelocity.scl(1, 0); // Set vertical velocity to zero
      }
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      // Update sprite position to match game object and draw
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      mSprite.draw(batch);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // Add gravity to velocity
      // Apply (scaled) gravity to player velocity
      mVelocity.add(mGravity.cpy().scl(delta));

      // Update position using velocity
      mPosition.add(mVelocity.cpy().scl(delta));

      // Set scale of sprite using velocity to face the right way
      if(mVelocity.x < 0)
         mSprite.setFlip(true, false);
      else
         mSprite.setFlip(false, false);

      // Update collider positon!
      mCollider.rect().setCenter(mPosition.x, mPosition.y);
   }
}
