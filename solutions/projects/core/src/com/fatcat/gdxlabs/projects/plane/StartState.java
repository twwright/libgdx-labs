package com.fatcat.gdxlabs.projects.plane;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.GameSprite;
import com.fatcat.gdxlabs.projects.game.GameWorld;
import com.fatcat.gdxlabs.projects.states.Button;
import com.fatcat.gdxlabs.projects.states.MenuState;
import com.fatcat.gdxlabs.projects.states.State;
import com.fatcat.gdxlabs.projects.states.StateChangeButton;

import java.util.List;

/**
 * Created by Tom on 24/02/2015.
 */
public class StartState extends MenuState
{
   private GameSprite mBackground;
   private GameSprite mTitle;
   private StateChangeButton mStartButton;
   private int mHighscore = 0;
   private Vector2 mScorePos;

   public StartState(String name, GameSprite background, GameSprite title, List<Button> buttons, StateChangeButton startButton, Vector2 scorePos)
   {
      super(name, Color.WHITE, buttons);
      mBackground = background;
      mTitle = title;
      mStartButton = startButton;
      mScorePos = scorePos;
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      super.update(world, delta);

      // Check for start from SPACE
      if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE))
         mStartButton.onPress(world);
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      super.draw(batch);

      // Render score as digits
      String score = String.valueOf(mHighscore);
      float padding = 5;
      Vector2 digitPos = mScorePos.cpy().add(padding, padding);
      for(int i = 0; i < score.length(); ++i)
      {
         int digit = Integer.parseInt(score.substring(i, i+1));
         TextureRegion texDigit = Resources.tDigits[digit];
         batch.draw(texDigit, digitPos.x, digitPos.y);
         digitPos.x += texDigit.getRegionWidth() + 5;
      }
   }

   @Override
   public void enter(State oldState, GameWorld world)
   {
      // Retrieve latest score and update highscore if necessary!
      int latestScore = ((PlaneWorld)world).getScore();
      if(latestScore > mHighscore)
         mHighscore = latestScore;

      // Add things
      world.add(mBackground);
      world.add(mTitle);

      super.enter(oldState, world);
   }

   @Override
   public void exit(State newState, GameWorld world)
   {
      super.exit(newState, world);

      // Remove things
      world.remove(mBackground);
      world.remove(mTitle);

   }
}
