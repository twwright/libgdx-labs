package com.fatcat.gdxlabs.projects.asteroids;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.*;

/**
 * Created by Tom on 21/02/2015.
 */
public class Ship extends GameObject implements IUpdateable, IDrawable, ICollidable, IShootable
{
   private final float MAX_VELOCITY = 300f;
   private Collider mCollider;
   private Sprite mSprite;

   private TextureRegion mLaserTexture;
   private Sound mLaserSound;

   private Vector2 mVelocity = new Vector2(0, 0);
   private float mDamping;
   private float mAcceleration;
   private float mRotationSpeed;
   private float mRotation; // Degrees

   private float mShootTimer = 0f;
   private float mShootTime = 1f;

   private boolean mDestroyed = false;
   private float mDestroyedTimer = 2f;
   private Sound mDestroyedSound;

   public Ship(Vector2 position, TextureRegion texture, TextureRegion laserTexture, Sound laserSound, Sound destroyedSound, float radius, float damping, float acceleration, float rotationSpeed, float rotation)
   {
      super(position);
      mSprite = new Sprite(texture);
      mLaserTexture = laserTexture;
      mLaserSound = laserSound;
      mDestroyedSound = destroyedSound;
      mCollider = new Collider(position.cpy(), radius);
      mDamping = damping;
      mAcceleration = acceleration;
      mRotationSpeed = rotationSpeed;
      mRotation = rotation;
   }

   @Override
   public Collider getCollider()
   {
      return mCollider;
   }

   @Override
   public boolean isSolid()
   {
      return true;
   }

   @Override
   public boolean isStatic()
   {
      return false;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      if(other.isSolid())
      {
         // Act like we got shot, and act like the asteroid got shot too!
         if(other instanceof IShootable)
            ((IShootable)other).onShot(world);
         onShot(world);
      }
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      // Set sprite position and rotation before drawing
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      mSprite.setRotation(mRotation);
      mSprite.draw(batch);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      if(mDestroyed)
         updateDestroyed(world, delta);
      else
         updateUndestroyed(world, delta);
   }

   private void updateDestroyed(GameWorld world, float delta)
   {
      if(mDestroyedTimer >= 0)
      {
         mRotation += mRotationSpeed * 2f * delta;
         mDestroyedTimer -= delta;
         if (mDestroyedTimer < 0)
         {
            // Destroyed timer has run out, change state back to menu!
            world.getStateManager().setNewState(AsteroidsGame.STATE_MENU);
         }
      }
   }

   private void updateUndestroyed(GameWorld world, float delta)
   {

      // Cast world to access Asteroids stuff
      AsteroidsWorld aWorld = (AsteroidsWorld)world;

      KeyBindings bindings = aWorld.getKeyBindings();

      // Handle turning
      if(bindings.isActionPressed(AsteroidsGame.KEY_LEFT))
      {
         mRotation += mRotationSpeed * delta;
      }
      else if(bindings.isActionPressed(AsteroidsGame.KEY_RIGHT))
      {
         mRotation -= mRotationSpeed * delta;
      }

      // Handle acceleration
      if(bindings.isActionPressed(AsteroidsGame.KEY_FORWARD))
      {
         // Get vector that is pointing in the direction of the rotation
         Vector2 acceleration = new Vector2((float)Math.cos(Math.toRadians(mRotation)), (float)Math.sin(Math.toRadians(mRotation)));
         acceleration.scl(mAcceleration * delta);
         mVelocity.add(acceleration);
      }
      else
      {
         // Apply damping with delta scaling!
         mVelocity.scl((float)Math.pow(mDamping, delta));
      }

      // Clamp velocity
      if(mVelocity.len() > MAX_VELOCITY)
         mVelocity.setLength(MAX_VELOCITY);

      // Handle shooting
      if(bindings.isActionPressed(AsteroidsGame.KEY_SHOOT) && mShootTimer <= 0f)
      {
         // Create laser and add just in front of ship
         Vector2 dir = new Vector2((float)Math.cos(Math.toRadians(mRotation)), (float)Math.sin(Math.toRadians(mRotation)));
         Vector2 laserPos = mPosition.cpy().add(dir.cpy().scl(65f)); // Place laser 50px in front of ship
         Vector2 laserVelocity = dir.cpy().scl(MAX_VELOCITY);

         Laser laser = new Laser(laserPos, mLaserTexture, laserVelocity, mRotation, mLaserTexture.getRegionHeight()/2);
         world.updateAdd(laser);

         // Play laser sound
         if(mLaserSound != null)
         {
            float pitchModifier = -0.2f + (float)Math.random() * 0.4f;
            mLaserSound.play(1f, 1f + pitchModifier, 0f);
         }
         mShootTimer = mShootTime;
      }

      // Update shooting timer
      if(mShootTimer > 0f)
         mShootTimer -= delta;

      // Modify position by velocity
      mPosition.add(mVelocity.cpy().scl(delta));

      // Wrap around vertically
      if(mPosition.y - mSprite.getHeight()/2 > world.size().y)
         mPosition.y = 0 - mSprite.getHeight()/2;
      else if(mPosition.y + mSprite.getHeight()/2 < 0)
         mPosition.y = world.size().y + mSprite.getHeight()/2;

      // Wrap around horizontally
      if(mPosition.x - mSprite.getWidth()/2 > world.size().x)
         mPosition.x = 0 - mSprite.getWidth()/2;
      else if(mPosition.x + mSprite.getWidth()/2 < 0)
         mPosition.x = world.size().x + mSprite.getWidth()/2;

      // Update collider position to match game object
      mCollider.circle().center(mPosition);
   }

   @Override
   public void onShot(GameWorld world)
   {
      // Play destroyed sound
      if(mDestroyedSound != null)
         mDestroyedSound.play();
      // Set destroyed (timer to restart game will start)
      mDestroyed = true;
   }
}
