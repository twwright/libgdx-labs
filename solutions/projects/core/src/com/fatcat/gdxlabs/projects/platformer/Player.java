package com.fatcat.gdxlabs.projects.platformer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.*;

/**
 * Created by Tom on 12/02/2015.
 */
public class Player extends GameObject implements IUpdateable, IDrawable, ICollidable
{
   public static final float MAX_VELOCITY = 500f;
   private Sprite mSprite;
   private TextureRegion mRegionStand;
   private TextureRegion mRegionJump;
   private TextureRegion mRegionHurt;

   private Camera mCamera;
   private GameSprite mBackground;

   private final Vector2 mVelocity = new Vector2(0, 0);
   private final Vector2 mGravity = new Vector2(0, -500f);
   private final Vector2 mJumpVelocity = new Vector2(0, 300f);
   private final float mAirControl = 0.5f;
   private final float mMoveVelocity = 250f;

   private boolean mIsAlive = true;

   private Collider mCollider;

   public Player(TextureRegion regionStand, TextureRegion regionJump, TextureRegion regionHurt, Vector2 position, Camera camera, GameSprite background)
   {
      super(position);
      mRegionStand = regionStand;
      mRegionJump = regionJump;
      mRegionHurt = regionHurt;
      mCamera = camera;
      mBackground = background;
      mSprite = new Sprite(mRegionStand);
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      mCollider = new Collider(mSprite.getBoundingRectangle());
   }

   @Override
   public Collider getCollider() {
      return mCollider;
   }

   @Override
   public boolean isSolid() {
      return mIsAlive;
   }

   @Override
   public boolean isStatic() {
      return false;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      // Only respond to a collision if we are alive
      if(mIsAlive && other.isSolid())
      {
         // Determine direction of collision and set velocity in that axis to 0
         Vector2 otherPos = ((GameObject) other).position();
         Vector2 directionToOther = otherPos.cpy().sub(mPosition).nor();
         Vector2 up = new Vector2(0, 1);

         // How similar is the direction to up?
         float dot = directionToOther.dot(up);

         // If dot is closer to -1 or 1 set vertical velocity to 0
         // else dot is closer to 0, set horizontal velocity to 0
         if (Math.abs(dot) > 0.5f)
            mVelocity.y = 0;
         else
            mVelocity.x = 0f;
      }
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      mSprite.draw(batch);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // Check if game is over! Fallen out of screen
      if(mPosition.y < 0)
      {
         // Play lose sound
         if(mIsAlive && Resources.sLose != null)
            Resources.sLose.play();

         // Reset the state!
         world.getStateManager().setNewState(PlatformerGame.STATE_GAME);
      }

      // Controls only enabled when alive
      if(mIsAlive)
      {
         KeyBindings bindings = ((PlatformerWorld)world).getBindings();
         // Check for jump (button pressed and on ground)
         if (bindings.isActionJustPressed(PlatformerGame.KEY_JUMP) && isGrounded())
         {
            // Play jump sound
            if(Resources.sJump != null)
               Resources.sJump.play();
            // Add our jump velocity to the player velocity
            mVelocity.add(mJumpVelocity);
         }

         // Apply movement left and right
         if (isGrounded()) {
            // If we are on the ground, stop moving if no key is pressed
            mVelocity.x = 0;
            // If left or right is pressed, set horizontal velocity to a constant speed
            if (bindings.isActionPressed(PlatformerGame.KEY_LEFT))
               mVelocity.x = -mMoveVelocity;
            if (bindings.isActionPressed(PlatformerGame.KEY_RIGHT))
               mVelocity.x = mMoveVelocity;
         } else {
            // If we are not on the ground, don't reset horizontal velocity
            // If left or right is pressed, add the move speed to the existing velocity
            if (bindings.isActionPressed(PlatformerGame.KEY_LEFT))
               mVelocity.add(new Vector2(-mMoveVelocity, 0).scl(mAirControl * delta));
            if (bindings.isActionPressed(PlatformerGame.KEY_RIGHT))
               mVelocity.add(new Vector2(mMoveVelocity, 0).scl(mAirControl * delta));
         }
      }

      // If travelling up or down, set to jumping sprite
      if(!mIsAlive)
         mSprite.setRegion(mRegionHurt);
      else if(mVelocity.y != 0)
         mSprite.setRegion(mRegionJump);
      else // Otherwise we must be on the ground, set the standing sprite
         mSprite.setRegion(mRegionStand);

      // Apply (scaled) gravity to player velocity
      Vector2 scaledGravity = mGravity.cpy().scl(delta);
      mVelocity.add(scaledGravity);

      // Clamp velocity to maximums (terminal velocity)
      if(mVelocity.x > MAX_VELOCITY)
         mVelocity.x = MAX_VELOCITY;
      if(mVelocity.y < -MAX_VELOCITY)
         mVelocity.y = -MAX_VELOCITY;
      if(mVelocity.y > MAX_VELOCITY)
         mVelocity.y = MAX_VELOCITY;
      if(mVelocity.y < -MAX_VELOCITY)
         mVelocity.y = -MAX_VELOCITY;

      // Apply player velocity to player position (scaled by delta)
      mPosition.add(mVelocity.cpy().scl(delta));

      // If moving left, flip the sprite
      if(mVelocity.x < 0)
         mSprite.setScale(-1, 1);
      else if(mVelocity.x > 0) // If moving right, don't flip the sprite
         mSprite.setScale(1, 1);

      // Update collider position
      mCollider.rect().setCenter(mPosition);

      // Set camera and background horizontal position
      mCamera.position.set(mPosition.x, mCamera.position.y, mCamera.position.z);
      mBackground.position().x = mPosition.x;
   }

   public boolean isGrounded()
   {
      return mVelocity.y == 0;
   }

   public boolean isAlive()
   {
      return mIsAlive;
   }

   public void setAlive(boolean alive)
   {
      mIsAlive = alive;
   }

   public void kill()
   {
      // Play lose sound
      if(Resources.sLose != null)
         Resources.sLose.play();

      // Not alive anymore
      setAlive(false);

      // Modify velocity
      mVelocity.set(mJumpVelocity.cpy().scl(0.5f));
   }
}
