package com.fatcat.gdxlabs.projects.game;

/**
 * Created by Tom on 11/02/2015.
 */
public interface IUpdateable
{
   public void update(GameWorld world, float delta);
}
