package com.fatcat.gdxlabs.projects.platformer;

import com.fatcat.gdxlabs.projects.game.GameWorld;

/**
 * Created by Tom on 25/02/2015.
 */
public abstract class Level
{
   public abstract void buildLevel(GameWorld world);
}
