package com.fatcat.gdxlabs.projects.platformer;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.*;

/**
 * Created by Tom on 12/02/2015.
 */
public class Block extends GameObject implements IDrawable, ICollidable
{
   private Sprite mSprite;
   private Collider mCollider;

   public Block(TextureRegion textureRegion, Vector2 position)
   {
      super(position);
      mSprite = new Sprite(textureRegion);
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      mCollider = new Collider(mSprite.getBoundingRectangle());
   }
   @Override
   public Collider getCollider()
   {
      return mCollider;
   }

   @Override
   public boolean isSolid()
   {
      return true;
   }

   @Override
   public boolean isStatic()
   {
      return true;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      // Do nothing
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      mSprite.draw(batch);
   }
}
