package com.fatcat.gdxlabs.projects.asteroids;

import com.fatcat.gdxlabs.projects.game.GameWorld;

/**
 * Created by Tom on 21/02/2015.
 */
public interface IShootable
{
   public void onShot(GameWorld world);
}
