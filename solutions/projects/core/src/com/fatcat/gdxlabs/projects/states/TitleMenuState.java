package com.fatcat.gdxlabs.projects.states;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.fatcat.gdxlabs.projects.game.GameSprite;
import com.fatcat.gdxlabs.projects.game.GameWorld;

import java.util.List;

/**
 * Created by Tom on 23/02/2015.
 */
public class TitleMenuState extends MenuState
{
   private GameSprite mTitle;
   private GameSprite mBackground;
   public TitleMenuState(String name, GameSprite background, List<Button> buttons, GameSprite title)
   {
      super(name, Color.WHITE, buttons);
      mTitle = title;
      mBackground = background;
   }

   public TitleMenuState(String name, Color background, List<Button> buttons, GameSprite title)
   {
      super(name, background, buttons);
      mTitle = title;
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      super.draw(batch);
      mTitle.draw(batch);
   }

   @Override
   public void exit(State newState, GameWorld world)
   {
      if(mBackground != null)
         world.remove(mBackground);
      super.exit(newState, world);
      world.remove(mTitle);
   }

   @Override
   public void enter(State oldState, GameWorld world)
   {
      if(mBackground != null)
         world.add(mBackground);
      super.enter(oldState, world);
      world.add(mTitle);
   }
}
