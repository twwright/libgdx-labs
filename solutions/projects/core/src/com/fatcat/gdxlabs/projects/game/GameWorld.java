package com.fatcat.gdxlabs.projects.game;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.states.StateManager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tom on 11/02/2015.
 */
public class GameWorld
{
   // Main collection of all game objects
   private List<GameObject> mGameObjects = new ArrayList<GameObject>();

   // Lists to add and remove game objects in update
   private List<GameObject> mAddedGameObjects = new LinkedList<GameObject>();
   private List<GameObject> mRemovedGameObjects = new LinkedList<GameObject>();

   // Specific collection of each game object composition element
   private List<IUpdateable> mUpdateables = new ArrayList<IUpdateable>();
   private List<ICollidable> mCollideables = new ArrayList<ICollidable>();
   private List<IDrawable> mDrawables = new ArrayList<IDrawable>();

   // Other information that needs to be shared with game objects and game
   private Camera mCamera;
   private StateManager mStateManager;
   private boolean mIsRunning = true;

   // Game world size
   private Vector2 mWorldSize;

   public GameWorld(Vector2 size, Camera camera, StateManager manager)
   {
      mCamera = camera;
      mStateManager = manager;
      mWorldSize = size;
   }

   public void add(GameObject obj)
   {
      mGameObjects.add(obj);
      if(obj instanceof IUpdateable)
         mUpdateables.add((IUpdateable)obj);
      if(obj instanceof ICollidable)
         mCollideables.add((ICollidable)obj);
      if(obj instanceof IDrawable)
         mDrawables.add((IDrawable)obj);
   }

   /*
    * Unused in these examples, but given as a reference!
    */
   public void remove(GameObject obj)
   {
      mGameObjects.remove(obj);

      if(obj instanceof IUpdateable)
         mUpdateables.remove((IUpdateable) obj);
      if(obj instanceof ICollidable)
         mCollideables.remove((ICollidable) obj);
      if(obj instanceof IDrawable)
         mDrawables.remove((IDrawable) obj);
   }

   public void updateRemove(GameObject obj)
   {
      mRemovedGameObjects.add(obj);
   }

   public void updateAdd(GameObject obj)
   {
      mAddedGameObjects.add(obj);
   }

   public void performUpdateAddRemove()
   {
      for(GameObject obj : mRemovedGameObjects)
         remove(obj);
      mRemovedGameObjects.clear();
      for(GameObject obj : mAddedGameObjects)
         add(obj);
      mAddedGameObjects.clear();
   }

   /*
    * Unused in these examples, but given as a reference!
    */
   public void removeInactives()
   {
      for(int i = mGameObjects.size()-1; i >= 0; --i)
      {
         GameObject obj = mGameObjects.get(i);
         if(!obj.isActive())
         {
            mGameObjects.remove(i);
            if(obj instanceof IUpdateable)
               mUpdateables.remove((IUpdateable) obj);
            if(obj instanceof ICollidable)
               mCollideables.remove((ICollidable) obj);
            if(obj instanceof IDrawable)
               mDrawables.remove((IDrawable) obj);
         }
      }
   }

   public void removeAll()
   {
      mGameObjects.clear();
      mUpdateables.clear();
      mDrawables.clear();
      mCollideables.clear();
   }

   public List<GameObject> getGameObjects() { return mGameObjects; }

   public List<IUpdateable> getUpdateables()
   {
      return mUpdateables;
   }

   public List<ICollidable> getCollideables()
   {
      return mCollideables;
   }

   public List<IDrawable> getDrawables()
   {
      return mDrawables;
   }

   public Vector2 size()
   {
      return mWorldSize;
   }

   public void setRunning(boolean running)
   {
      mIsRunning = running;
   }

   public boolean isRunning()
   {
      return mIsRunning;
   }

   public Camera getCamera()
   {
      return mCamera;
   }

   public StateManager getStateManager()
   {
      return mStateManager;
   }
}
