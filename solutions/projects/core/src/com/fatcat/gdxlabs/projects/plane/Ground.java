package com.fatcat.gdxlabs.projects.plane;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.GameObject;
import com.fatcat.gdxlabs.projects.game.GameWorld;
import com.fatcat.gdxlabs.projects.game.IDrawable;
import com.fatcat.gdxlabs.projects.game.IUpdateable;

/**
 * Created by Tom on 24/02/2015.
 */
public class Ground extends GameObject implements IDrawable, IUpdateable
{
   private Sprite mSprite;
   private float mVelocity;
   private float mResetX;

   public Ground(Vector2 position, TextureRegion texture, float horizontalVelocity, float resetX)
   {
      super(position);
      mSprite = new Sprite(texture);
      mVelocity = horizontalVelocity;
      mResetX = resetX;
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      // Update sprite position then draw
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      mSprite.draw(batch);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // Update position with velocity
      mPosition.x += mVelocity * delta;

      // Check if block has left game window
      if(mPosition.x + mSprite.getWidth()/2 < 0)
      {
         // Reset position to the resetX
         mPosition.x += mResetX;
      }
   }
}
