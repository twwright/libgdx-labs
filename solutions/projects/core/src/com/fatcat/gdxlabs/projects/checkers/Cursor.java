package com.fatcat.gdxlabs.projects.checkers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.fatcat.gdxlabs.projects.game.GameObject;
import com.fatcat.gdxlabs.projects.game.GameWorld;
import com.fatcat.gdxlabs.projects.game.IDrawable;
import com.fatcat.gdxlabs.projects.game.IUpdateable;

/**
 * Created by Tom on 28/02/2015.
 */
public class Cursor extends GameObject implements IUpdateable, IDrawable
{
   private Sprite mSprite;
   private TextureRegion mNoPlayer;
   private TextureRegion mPlayerOne;
   private TextureRegion mPlayerTwo;

   public Cursor(Vector2 position, TextureRegion noPlayer, TextureRegion playerOne, TextureRegion playerTwo)
   {
      super(position);
      mSprite = new Sprite(noPlayer);
      mNoPlayer = noPlayer;
      mPlayerOne = playerOne;
      mPlayerTwo = playerTwo;
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      mSprite.draw(batch);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // Retrieve touch coordinates and unproject with camera
      Vector3 screenCoords = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
      world.getCamera().unproject(screenCoords);

      // Set position to mouse position
      mPosition.set(screenCoords.x, screenCoords.y);

      CheckersWorld cWorld = (CheckersWorld)world;

      if(cWorld.getPlayer() == CheckersState.PLAYER_ONE)
         mSprite.setRegion(mPlayerOne);
      else if(cWorld.getPlayer() == CheckersState.PLAYER_TWO)
         mSprite.setRegion(mPlayerTwo);
      else
         mSprite.setRegion(mNoPlayer);
   }
}
