package com.fatcat.gdxlabs.projects.checkers;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Tom on 27/02/2015.
 */
public class Resources
{
   public static TextureRegion tSquareLight;
   public static TextureRegion tSquareDark;
   public static TextureRegion tPieceLight;
   public static TextureRegion tPieceDark;
   public static TextureRegion tKingLight;
   public static TextureRegion tKingDark;
   public static TextureRegion tMove;
   public static TextureRegion tCursor;
   public static TextureRegion tCursorLight;
   public static TextureRegion tCursorDark;

   public static TextureRegion tRestart;
   public static TextureRegion tRestartSelected;
   public static TextureRegion tQuit;
   public static TextureRegion tQuitSelected;

   public static Sound sMove;
   public static Sound sTake;
   public static Sound sWin;
   public static Sound sBadClick;
   public static Sound sKing;
}
