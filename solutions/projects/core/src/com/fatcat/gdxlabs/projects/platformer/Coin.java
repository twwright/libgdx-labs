package com.fatcat.gdxlabs.projects.platformer;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.*;

/**
 * Created by Tom on 25/02/2015.
 */
public class Coin extends GameObject implements IDrawable, ICollidable
{
   private Collider mCollider;
   private Sprite mSprite;

   public Coin(Vector2 position, TextureRegion textureRegion) {
      super(position);
      mSprite = new Sprite(textureRegion);
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      mCollider = new Collider(mSprite.getBoundingRectangle());
   }

   @Override
   public Collider getCollider()
   {
      return mCollider ;
   }

   @Override
   public boolean isSolid() {
      return false;
   }

   @Override
   public boolean isStatic()
   {
      return true;
   }

   @Override
   public void onCollide(ICollidable other, GameWorld world)
   {
      if(other instanceof Player && ((Player)other).isAlive())
      {
         // Play coin sound
         if(Resources.sCoin != null)
            Resources.sCoin.play();

         // Increase coin count in world
         ((PlatformerWorld)world).addCoins(1);

         // Remove self from world
         world.updateRemove(this);
      }
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      mSprite.draw(batch);
   }
}
