package com.fatcat.gdxlabs.projects.asteroids;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.*;
import com.fatcat.gdxlabs.projects.states.*;

import java.util.LinkedList;
import java.util.List;

public class AsteroidsGame extends CompositionStateGame
{
	public static final String KEY_LEFT = "turnLeft";
	public static final String KEY_RIGHT = "turnRight";
	public static final String KEY_FORWARD = "forward";
	public static final String KEY_SHOOT = "shoot";
	public static final String KEY_PAUSE = "pause";

	public static final String STATE_MENU = "menu";
	public static final String STATE_GAME = "game";
	public static final String STATE_PAUSE = "pause";

	public static final int NUM_ASTEROIDS = 4;
	public static final int NUM_STARS = 15;
	public static final int SPAWN_BORDER = 50;

	private Texture mAtlas;

	private Texture mButtonAtlas;
	private TextureRegion mRegionTitle;
	private TextureRegion mRegionPauseTitle;
	private TextureRegion mRegionPlay;
	private TextureRegion mRegionPlaySelected;
	private TextureRegion mRegionBack;
	private TextureRegion mRegionBackSelected;
	private TextureRegion mRegionQuit;
	private TextureRegion mRegionQuitSelected;

	@Override
	public void create()
	{
		super.create();
		mCollision = new Collision(Collision.CIRCLE_MODE);

		KeyBindings bindings = new KeyBindings();
		bindings.setBinding(KEY_LEFT, Input.Keys.LEFT);
		bindings.setBinding(KEY_RIGHT, Input.Keys.RIGHT);
		bindings.setBinding(KEY_FORWARD, Input.Keys.UP);
		bindings.setBinding(KEY_SHOOT, Input.Keys.SPACE);
		bindings.setBinding(KEY_PAUSE, Input.Keys.ESCAPE);
		mWorld = new AsteroidsWorld(mWorld, bindings);

		// Load assets
		mAtlas = new Texture(Gdx.files.internal("atlasAsteroids.png"));
		Resources.tShip = new TextureRegion(mAtlas, 0, 0, 83, 103);
		Resources.tLaser = new TextureRegion(mAtlas, 189, 109, 56, 19);
		Resources.tStar = new TextureRegion(mAtlas, 181, 109, 8, 8);
		Resources.tAsteroidL = new TextureRegion(mAtlas, 83, 0, 98, 96);
		Resources.tAsteroidM = new TextureRegion(mAtlas, 181, 0, 75, 68);
		Resources.tAsteroidS = new TextureRegion(mAtlas, 181, 68, 45, 41);

		mButtonAtlas = new Texture(Gdx.files.internal("atlasAsteroidsButtons.png"));
		mRegionTitle = new TextureRegion(mButtonAtlas, 0, 0, 450, 75);
		mRegionPauseTitle = new TextureRegion(mButtonAtlas, 0, 300, 450, 75);
		mRegionPlay = new TextureRegion(mButtonAtlas, 0, 75, 225, 75);
		mRegionBack = new TextureRegion(mButtonAtlas, 0, 150, 225, 75);
		mRegionQuit = new TextureRegion(mButtonAtlas, 0, 225, 225, 75);
		mRegionPlaySelected = new TextureRegion(mButtonAtlas, 225, 75, 225, 75);
		mRegionBackSelected = new TextureRegion(mButtonAtlas, 225, 150, 225, 75);
		mRegionQuitSelected = new TextureRegion(mButtonAtlas, 225, 225, 225, 75);

		// Load sound assets
		Resources.sClick = Gdx.audio.newSound(Gdx.files.internal("click.mp3"));
		Resources.sShoot = Gdx.audio.newSound(Gdx.files.internal("laser.wav"));
		Resources.sExplode = Gdx.audio.newSound(Gdx.files.internal("explode.wav"));
		Resources.sBounce = Gdx.audio.newSound(Gdx.files.internal("bounce.wav"));
		Resources.sHit = Gdx.audio.newSound(Gdx.files.internal("hit.wav"));

		// Create menu state
		List<Button> menuButtons = new LinkedList<Button>();
		float centerX = mWorld.size().x / 2, centerY = mWorld.size().y / 2;
		menuButtons.add(new StateChangeButton(new Vector2(centerX, centerY + 60), mRegionPlay, mRegionPlaySelected, STATE_GAME, Resources.sClick));
		menuButtons.add(new ExitButton(new Vector2(centerX, centerY - 60), mRegionQuit, mRegionQuitSelected, Resources.sClick));
		GameSprite menuTitle = new GameSprite(new Vector2(centerX, centerY + 200), mRegionTitle);
		TitleMenuState menuState = new TitleMenuState(STATE_MENU, Color.BLACK, menuButtons, menuTitle);
		mStateManager.registerState(menuState);

		// Create pause state
		List<Button> pauseButtons = new LinkedList<Button>();
		pauseButtons.add(new StateChangeButton(new Vector2(centerX, centerY + 60), mRegionBack, mRegionBackSelected, STATE_GAME, Resources.sClick));
		pauseButtons.add(new StateChangeButton(new Vector2(centerX, centerY - 60), mRegionQuit, mRegionQuitSelected, STATE_MENU, Resources.sClick));
		GameSprite pauseTitle = new GameSprite(new Vector2(centerX, centerY + 200), mRegionPauseTitle);
		TitleMenuState pauseState = new TitleMenuState(STATE_PAUSE, Color.BLACK, pauseButtons, pauseTitle);
		mStateManager.registerState(pauseState);

		// Create game state
		AsteroidsState gameState = new AsteroidsState(STATE_GAME, Color.PURPLE);
		mStateManager.registerState(gameState);

		mStateManager.setNewState(STATE_MENU);
	}

	@Override
	public void initialiseGame()
	{
		// Nothing?
	}

	@Override
	public void dispose()
	{
		super.dispose();
		mAtlas.dispose();
		mButtonAtlas.dispose();
		Resources.sBounce.dispose();
		Resources.sClick.dispose();
		Resources.sExplode.dispose();
		Resources.sHit.dispose();
		Resources.sShoot.dispose();
	}
}
