package com.fatcat.gdxlabs.projects.plane;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.Collision;
import com.fatcat.gdxlabs.projects.game.CompositionStateGame;
import com.fatcat.gdxlabs.projects.game.GameSprite;
import com.fatcat.gdxlabs.projects.states.Button;
import com.fatcat.gdxlabs.projects.states.ExitButton;
import com.fatcat.gdxlabs.projects.states.StateChangeButton;
import com.fatcat.gdxlabs.projects.states.TitleMenuState;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tom on 24/02/2015.
 */
public class PlaneGame extends CompositionStateGame
{
   public static final String STATE_MENU = "menu";
   public static final String STATE_READY = "ready";
   public static final String STATE_PLAY = "game";

   private Texture mAtlas;
   private Texture mAtlasTitles;

   @Override
   public void create()
   {
      super.create();

      // Create collision
      mCollision = new Collision(Collision.RECTANGLE_MODE);

      // Show colliders
      //CompositionStateGame.colliderDrawEnabled = true;

      // Load assets
      mAtlas = new Texture(Gdx.files.internal("planeAtlas.png"));
      Resources.tPlane = new TextureRegion(mAtlas, 113, 0, 88, 73);
      Resources.tGround = new TextureRegion(mAtlas, 113, 73, 70, 70);
      Resources.tPipe = new TextureRegion(mAtlas, 0, 0, 113, 512);
      Resources.tBackground = new TextureRegion(mAtlas, 224, 0, 800, 480);
      Resources.tDigits = new TextureRegion[] {
            new TextureRegion(mAtlas, 113, 143, 53, 77), // 0
            new TextureRegion(mAtlas, 166, 143, 37, 75), // 1
            new TextureRegion(mAtlas, 113, 220, 51, 76), // 2
            new TextureRegion(mAtlas, 164, 218, 51, 78), // 3
            new TextureRegion(mAtlas, 113, 296, 55, 71), // 4
            new TextureRegion(mAtlas, 168, 296, 50, 72), // 5
            new TextureRegion(mAtlas, 113, 367, 53, 72), // 6
            new TextureRegion(mAtlas, 166, 368, 51, 72), // 7
            new TextureRegion(mAtlas, 113, 439, 51, 73), // 8
            new TextureRegion(mAtlas, 164, 440, 53, 72), // 9
      };

      mAtlasTitles = new Texture(Gdx.files.internal("planeAtlasTitles.png"));
      Resources.tGameOver = new TextureRegion(mAtlasTitles, 0, 0, 412, 73);
      Resources.tGetReady = new TextureRegion(mAtlasTitles, 0, 73, 400, 248);
      Resources.tStart = new TextureRegion(mAtlasTitles, 0, 330, 225, 75);
      Resources.tStartSelected = new TextureRegion(mAtlasTitles, 225, 330, 225, 75);
      Resources.tQuit = new TextureRegion(mAtlasTitles, 0, 405, 225, 75);
      Resources.tQuitSelected = new TextureRegion(mAtlasTitles, 225, 405, 225, 75);

      // Load sounds
      Resources.sLose = Gdx.audio.newSound(Gdx.files.internal("lose.wav"));
      Resources.sScore = Gdx.audio.newSound(Gdx.files.internal("score.wav"));
      Resources.sSelect = Gdx.audio.newSound(Gdx.files.internal("select.wav"));

      // Recreate world to be a plane world
      mWorld = new PlaneWorld(mWorld);

      // Create states
      List<Button> menuButtons = new LinkedList<Button>();
      StateChangeButton startButton = new StateChangeButton(new Vector2(mWorld.size().x/4, mWorld.size().y/4), Resources.tStart, Resources.tStartSelected, STATE_PLAY, Resources.sSelect);
      menuButtons.add(startButton);
      menuButtons.add(new ExitButton(new Vector2((mWorld.size().x/4) * 3, mWorld.size().y/4), Resources.tQuit, Resources.tQuitSelected, Resources.sSelect));
      GameSprite menuTitle = new GameSprite(new Vector2(mWorld.size().x/2, mWorld.size().y/2), Resources.tGetReady);
      GameSprite menuBackground = new GameSprite(mWorld.size().cpy().scl(0.5f), Resources.tBackground);
      StartState startState = new StartState(STATE_MENU, menuBackground, menuTitle, menuButtons, startButton, new Vector2(10f, 10f));
      mStateManager.registerState(startState);

      PlaneState planeState = new PlaneState(STATE_PLAY, Resources.tDigits, Vector2.Zero, 200f, 175, 100f, 450f);
      mStateManager.registerState(planeState);

      initialiseGame();
   }

   @Override
   public void initialiseGame()
   {
      mStateManager.setNewState(STATE_MENU);
   }

   @Override
   public void dispose()
   {
      super.dispose();
      mAtlas.dispose();
      mAtlasTitles.dispose();
      Resources.sLose.dispose();
      Resources.sScore.dispose();
      Resources.sSelect.dispose();
   }
}
