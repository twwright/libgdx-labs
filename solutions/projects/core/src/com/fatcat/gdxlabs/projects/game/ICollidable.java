package com.fatcat.gdxlabs.projects.game;

/**
 * Created by Tom on 11/02/2015.
 */
public interface ICollidable
{
   public Collider getCollider();
   public boolean isSolid();
   public boolean isStatic();
   public void onCollide(ICollidable other, GameWorld world);
}
