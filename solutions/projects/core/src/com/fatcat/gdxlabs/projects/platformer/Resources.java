package com.fatcat.gdxlabs.projects.platformer;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Tom on 25/02/2015.
 */
public class Resources
{
   public static TextureRegion tPlayer;
   public static TextureRegion tPlayerJump;
   public static TextureRegion tPlayerHurt;
   public static TextureRegion tBlockDirt;
   public static TextureRegion tBlockGrass;
   public static TextureRegion tFence;
   public static TextureRegion tPlant;
   public static TextureRegion tBlob;
   public static TextureRegion tCoin;
   public static TextureRegion tFlag;
   public static TextureRegion tBackground;
   public static TextureRegion tDoor;
   public static TextureRegion tDoorLocked;
   public static TextureRegion[] tDigits;

   public static TextureRegion tPauseTitle;
   public static TextureRegion tMenuTitle;
   public static TextureRegion tBack;
   public static TextureRegion tBackSelected;
   public static TextureRegion tQuit;
   public static TextureRegion tQuitSelected;
   public static TextureRegion tPlay;
   public static TextureRegion tPlaySelected;

   public static Sound sJump;
   public static Sound sCoin;
   public static Sound sLose;
   public static Sound sFinish;
   public static Sound sSelect;
}
