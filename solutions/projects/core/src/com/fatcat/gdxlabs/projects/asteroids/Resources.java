package com.fatcat.gdxlabs.projects.asteroids;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by Tom on 23/02/2015.
 */
public class Resources
{
   // Texture Regions
   public static TextureRegion tAsteroidL;
   public static TextureRegion tAsteroidM;
   public static TextureRegion tAsteroidS;
   public static TextureRegion tStar;
   public static TextureRegion tShip;
   public static TextureRegion tLaser;

   // Sounds
   public static Sound sShoot;
   public static Sound sBounce;
   public static Sound sHit;
   public static Sound sClick;
   public static Sound sExplode;
}
