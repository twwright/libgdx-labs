package com.fatcat.gdxlabs.projects.plane;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.GameWorld;
import com.fatcat.gdxlabs.projects.states.StateManager;

/**
 * Created by Tom on 24/02/2015.
 */
public class PlaneWorld extends GameWorld
{
   private int mScore = 0;

   public PlaneWorld(GameWorld world)
   {
      this(world.size(), world.getCamera(), world.getStateManager());
   }

   public PlaneWorld(Vector2 size, Camera camera, StateManager manager)
   {
      super(size, camera, manager);
   }

   public int getScore()
   {
      return mScore;
   }

   public void setScore(int score)
   {
      mScore = score;
   }
}
