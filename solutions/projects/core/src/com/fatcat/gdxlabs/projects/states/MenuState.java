package com.fatcat.gdxlabs.projects.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.fatcat.gdxlabs.projects.game.GameWorld;

import java.util.List;

/**
 * Created by Tom on 15/02/2015.
 */
public class MenuState extends State
{
   private Color mBackgroundColour;
   private List<Button> mButtons;

   public MenuState(String name, Color background, List<Button> buttons)
   {
      super(name);
      mBackgroundColour = background;
      mButtons = buttons;
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // Clear the screen with our background color!
      Gdx.gl.glClearColor(mBackgroundColour.r, mBackgroundColour.g, mBackgroundColour.b, mBackgroundColour.a);
      Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

      // Do nothing else, buttons update themselves
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      // Do nothing, buttons draw themselves
   }

   @Override
   public void enter(State oldState, GameWorld world)
   {
      for(Button button : mButtons)
      {
         button.setSelected(false);
         world.add(button);
      }
   }

   @Override
   public void exit(State newState, GameWorld world)
   {
      for(Button button : mButtons)
         world.remove(button);
   }
}
