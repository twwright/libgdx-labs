package com.fatcat.gdxlabs.projects.plane;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Tom on 24/02/2015.
 */
public class Resources
{
   public static TextureRegion tPlane;
   public static TextureRegion tGround;
   public static TextureRegion tPipe;
   public static TextureRegion tBackground;
   public static TextureRegion[] tDigits;

   public static TextureRegion tGameOver;
   public static TextureRegion tGetReady;
   public static TextureRegion tStart;
   public static TextureRegion tStartSelected;
   public static TextureRegion tQuit;
   public static TextureRegion tQuitSelected;

   public static Sound sScore;
   public static Sound sLose;
   public static Sound sJump;
   public static Sound sSelect;
}
