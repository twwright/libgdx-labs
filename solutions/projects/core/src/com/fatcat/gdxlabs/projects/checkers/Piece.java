package com.fatcat.gdxlabs.projects.checkers;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.*;

/**
 * Created by Tom on 28/02/2015.
 */
public class Piece extends GameObject implements IUpdateable, IDrawable
{
   private Sprite mSprite;

   private int mPlayer;
   private TextureRegion mNormal;
   private TextureRegion mKing;

   private boolean mIsKing = false;
   private boolean mIsSelected = false;

   private float mRotationSpeed;
   private float mScale;

   public Piece(Vector2 position, int player, TextureRegion normalTexture, TextureRegion kingTexture, float selectedRotationSpeed, float selectedScale)
   {
      super(position);
      mPlayer = player;
      mNormal = normalTexture;
      mKing = kingTexture;
      mRotationSpeed = selectedRotationSpeed;
      mScale = selectedScale;
      mSprite = new Sprite(normalTexture);
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      mSprite.setPosition(mPosition.x - mSprite.getWidth()/2, mPosition.y - mSprite.getHeight()/2);
      mSprite.draw(batch);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      if(mIsSelected)
      {
         mSprite.rotate(mRotationSpeed * delta);
         mSprite.setScale(mScale);
      }
      else
      {
         mSprite.setRotation(0);
         mSprite.setScale(1f);
      }
   }

   public boolean isKing()
   {
      return mIsKing;
   }

   public void setKing(boolean isKing)
   {
      mIsKing = isKing;
      mSprite.setRegion(isKing ? mKing : mNormal);
   }

   public boolean isSelected()
   {
      return mIsSelected;
   }

   public void setSelected(boolean isSelected)
   {
      mIsSelected = isSelected;
   }

   public int getPlayer()
   {
      return mPlayer;
   }
}
