package com.fatcat.gdxlabs.projects.platformer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.fatcat.gdxlabs.projects.game.*;
import com.fatcat.gdxlabs.projects.states.Button;
import com.fatcat.gdxlabs.projects.states.ExitButton;
import com.fatcat.gdxlabs.projects.states.StateChangeButton;
import com.fatcat.gdxlabs.projects.states.TitleMenuState;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tom on 25/02/2015.
 */
public class PlatformerGame extends CompositionStateGame
{
   public static final String STATE_PAUSE = "pause";
   public static final String STATE_GAME = "game";
   public static final String STATE_MENU = "menu";

   public static final String KEY_PAUSE = "pause";
   public static final String KEY_LEFT = "left";
   public static final String KEY_RIGHT = "right";
   public static final String KEY_JUMP = "jump";

   private Texture mAtlas;
   private Texture mAtlasBackground;
   private Texture mAtlasDigits;
   private Texture mAtlasButtons;

   @Override
   public void create()
   {
      super.create();

      // Create collision
      mCollision = new Collision(Collision.RECTANGLE_MODE);
      //CompositionStateGame.colliderDrawEnabled = true;

      // Load assets
      mAtlas = new Texture(Gdx.files.internal("platformerAtlas.png"));
      Resources.tPlayer = new TextureRegion(mAtlas, 0, 0, 66, 92);
      Resources.tPlayerJump = new TextureRegion(mAtlas, 0, 92, 67, 94);
      Resources.tPlayerHurt = new TextureRegion(mAtlas, 67, 70, 69, 92);
      Resources.tBlockGrass = new TextureRegion(mAtlas, 66, 0, 70, 70);
      Resources.tBlockDirt = new TextureRegion(mAtlas, 136, 0, 70, 70);
      Resources.tCoin = new TextureRegion(mAtlas, 206, 0, 36, 36);
      Resources.tFence = new TextureRegion(mAtlas, 136, 70, 70, 61);
      Resources.tPlant = new TextureRegion(mAtlas, 206, 52, 48, 106);
      Resources.tBlob = new TextureRegion(mAtlas, 136, 131, 70, 39);
      Resources.tFlag = new TextureRegion(mAtlas, 0, 186, 67, 70);
      Resources.tDoor = new TextureRegion(mAtlas, 67, 170, 70, 86);
      Resources.tDoorLocked = new TextureRegion(mAtlas, 137, 170, 70, 86);
      mAtlasBackground = new Texture(Gdx.files.internal("platformerAtlasBackground.png"));
      Resources.tBackground = new TextureRegion(mAtlasBackground, 0, 0, 800, 480);
      mAtlasDigits = new Texture(Gdx.files.internal("numbersAtlas.png"));
      Resources.tDigits = new TextureRegion[10];
      for(int i = 0; i < 10; ++i)
      {
         Resources.tDigits[i] = new TextureRegion(mAtlasDigits, i * 35, 0, 35, 44);
      }
      mAtlasButtons = new Texture(Gdx.files.internal("platformerAtlasButtons.png"));
      Resources.tPauseTitle = new TextureRegion(mAtlasButtons, 0, 0, 450, 75);
      Resources.tMenuTitle = new TextureRegion(mAtlasButtons, 0, 75, 450, 75);
      Resources.tPlay = new TextureRegion(mAtlasButtons, 0, 150, 225, 75);
      Resources.tPlaySelected = new TextureRegion(mAtlasButtons, 225, 150, 225, 75);
      Resources.tBack = new TextureRegion(mAtlasButtons, 0, 225, 225, 75);
      Resources.tBackSelected = new TextureRegion(mAtlasButtons, 225, 225, 225, 75);
      Resources.tQuit = new TextureRegion(mAtlasButtons, 0, 300, 225, 75);
      Resources.tQuitSelected = new TextureRegion(mAtlasButtons, 225, 300, 225, 75);

      Resources.sLose = Gdx.audio.newSound(Gdx.files.internal("lose.wav"));
      Resources.sCoin = Gdx.audio.newSound(Gdx.files.internal("score.wav"));
      Resources.sSelect = Gdx.audio.newSound(Gdx.files.internal("select.wav"));
      Resources.sJump = Gdx.audio.newSound(Gdx.files.internal("click.mp3"));
      Resources.sFinish = Gdx.audio.newSound(Gdx.files.internal("select.wav"));

      // Create levels
      List<Level> levelList = createLevels();

      // Create key bindings (first create defaults in case load fails)
      KeyBindings bindings = new KeyBindings();
      bindings.setBinding(PlatformerGame.KEY_JUMP, Input.Keys.SPACE);
      bindings.setBinding(PlatformerGame.KEY_LEFT, Input.Keys.LEFT);
      bindings.setBinding(PlatformerGame.KEY_RIGHT, Input.Keys.RIGHT);
      bindings.setBinding(PlatformerGame.KEY_PAUSE, Input.Keys.ESCAPE);
      try
      {
         bindings = KeyBindings.loadFromFile("platformerControls.txt");
      } catch (FileNotFoundException e) { /* Nothing */ }

      // Recreate world
      mWorld = new PlatformerWorld(mWorld, levelList, bindings);

      // Create states
      Vector2 screenCenter = mWorld.size().cpy().scl(0.5f);
      GameSprite background = new GameSprite(screenCenter, Resources.tBackground);

      List<Button> pauseButtons = new ArrayList<Button>();
      pauseButtons.add(new StateChangeButton(screenCenter.cpy().add(-150, 0), Resources.tBack, Resources.tBackSelected, STATE_GAME, Resources.sSelect));
      pauseButtons.add(new StateChangeButton(screenCenter.cpy().add(150, 0), Resources.tQuit, Resources.tQuitSelected, STATE_MENU, Resources.sSelect));
      GameSprite pauseTitle = new GameSprite(screenCenter.cpy().add(0, 150), Resources.tPauseTitle);
      TitleMenuState pauseState = new TitleMenuState(STATE_PAUSE, background, pauseButtons, pauseTitle);
      mStateManager.registerState(pauseState);

      List<Button> menuButtons = new ArrayList<Button>();
      menuButtons.add(new StateChangeButton(screenCenter.cpy().add(-150, 0), Resources.tPlay, Resources.tPlaySelected, STATE_GAME, Resources.sSelect));
      menuButtons.add(new ExitButton(screenCenter.cpy().add(150, 0), Resources.tQuit, Resources.tQuitSelected, Resources.sSelect));
      GameSprite menuTitle = new GameSprite(screenCenter.cpy().add(0, 150), Resources.tMenuTitle);
      TitleMenuState menuState = new TitleMenuState(STATE_MENU, background, menuButtons, menuTitle);
      mStateManager.registerState(menuState);

      PlatformerState gameState = new PlatformerState(STATE_GAME);
      mStateManager.registerState(gameState);

      initialiseGame();
   }
   @Override
   public void initialiseGame()
   {
      mWorld.removeAll();

      ((PlatformerWorld)mWorld).setCurrentLevelIndex(0);
      mStateManager.setNewState(STATE_MENU);
   }

   @Override
   public void dispose()
   {
      mAtlas.dispose();
      mAtlasBackground.dispose();
      mAtlasDigits.dispose();
      mAtlasButtons.dispose();
      Resources.sCoin.dispose();
      Resources.sJump.dispose();
      Resources.sLose.dispose();
      Resources.sSelect.dispose();
      Resources.sFinish.dispose();
   }

   private List<Level> createLevels()
   {
      List<Level> levelList = new LinkedList<Level>();

      // Level 1
      levelList.add(new Level()
      {
         @Override
         public void buildLevel(GameWorld world)
         {
            // Background
            GameSprite background = new GameSprite(new Vector2(world.size().x/2, world.size().y/2), Resources.tBackground);
            world.add(background);

            // Props
            world.add(new GameSprite(new Vector2(140, 110), Resources.tPlant));
            world.add(new GameSprite(new Vector2(680, 110), Resources.tFence));

            // Finish
            world.add(new LevelFinish(new Vector2(1120, 120), Resources.tDoor, Resources.tDoorLocked, Resources.tDigits, 5));

            // Blocks
            world.add(new Block(Resources.tBlockGrass, new Vector2(-70, 50)));
            world.add(new Block(Resources.tBlockGrass, new Vector2(0, 50)));
            world.add(new Block(Resources.tBlockGrass, new Vector2(70, 50)));
            world.add(new Block(Resources.tBlockGrass, new Vector2(140, 50)));

            world.add(new Block(Resources.tBlockDirt, new Vector2(-35, 120)));
            world.add(new Block(Resources.tBlockDirt, new Vector2(-35, 190)));
            world.add(new Block(Resources.tBlockGrass, new Vector2(-35, 260)));

            world.add(new Block(Resources.tBlockGrass, new Vector2(350, 50)));
            world.add(new Coin(new Vector2(350, 200), Resources.tCoin));

            world.add(new Block(Resources.tBlockDirt, new Vector2(560, 50)));
            world.add(new Block(Resources.tBlockGrass, new Vector2(560, 120)));
            world.add(new Block(Resources.tBlockGrass, new Vector2(630, 50)));
            world.add(new Block(Resources.tBlockGrass, new Vector2(700, 50)));
            world.add(new Block(Resources.tBlockGrass, new Vector2(770, 50)));
            world.add(new Block(Resources.tBlockGrass, new Vector2(840, 50)));
            world.add(new Block(Resources.tBlockGrass, new Vector2(910, 50)));
            world.add(new Block(Resources.tBlockGrass, new Vector2(945, 120)));
            world.add(new Block(Resources.tBlockGrass, new Vector2(980, 50)));
            world.add(new Block(Resources.tBlockGrass, new Vector2(1050, 50)));
            world.add(new Block(Resources.tBlockGrass, new Vector2(1120, 50)));
            world.add(new Coin(new Vector2(630, 120), Resources.tCoin));
            world.add(new Coin(new Vector2(700, 120), Resources.tCoin));
            world.add(new Coin(new Vector2(770, 120), Resources.tCoin));
            world.add(new Coin(new Vector2(840, 120), Resources.tCoin));

            // Enemies
            world.add(new Enemy(Resources.tBlob, new Vector2(800, 150), new Vector2(-50, 0)));
            world.add(new Enemy(Resources.tBlob, new Vector2(400, 130), new Vector2(-50, 0)));

            // Player
            world.add(new Player(Resources.tPlayer, Resources.tPlayerJump, Resources.tPlayerHurt, new Vector2(140, 200), mCamera, background));
         }
      });

      // Level 2
      levelList.add(new Level()
      {
         @Override
         public void buildLevel(GameWorld world)
         {
            // Background
            GameSprite background = new GameSprite(new Vector2(world.size().x/2, world.size().y/2), Resources.tBackground);
            world.add(background);

            // Player
            world.add(new Player(Resources.tPlayer, Resources.tPlayerJump, Resources.tPlayerHurt, new Vector2(140, 200), mCamera, background));
         }
      });

      // Level 3
      levelList.add(new Level()
      {
         @Override
         public void buildLevel(GameWorld world)
         {
            // Background
            GameSprite background = new GameSprite(new Vector2(world.size().x/2, world.size().y/2), Resources.tBackground);
            world.add(background);

            // Player
            world.add(new Player(Resources.tPlayer, Resources.tPlayerJump, Resources.tPlayerHurt, new Vector2(140, 200), mCamera, background));
         }
      });
      return levelList;
   }
}
