package com.fatcat.gdxlabs.projects.platformer;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.fatcat.gdxlabs.projects.game.GameObject;
import com.fatcat.gdxlabs.projects.game.GameWorld;
import com.fatcat.gdxlabs.projects.game.KeyBindings;
import com.fatcat.gdxlabs.projects.states.State;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tom on 25/02/2015.
 */
public class PlatformerState extends State
{
   private List<GameObject> mStoredGameObjects = new LinkedList<GameObject>();

   public PlatformerState(String name)
   {
      super(name);
   }

   @Override
   public void update(GameWorld world, float delta)
   {
      // Check for pause
      KeyBindings bindings = ((PlatformerWorld)world).getBindings();
      if(bindings.isActionJustPressed(PlatformerGame.KEY_PAUSE))
      {
         world.getStateManager().setNewState(PlatformerGame.STATE_PAUSE);
      }
   }

   @Override
   public void draw(SpriteBatch batch)
   {
      // Nothing
   }

   @Override
   public void enter(State oldState, GameWorld world)
   {
      // Ensure world is empty
      world.removeAll();

      // Reset the level if coming from menu
      if(oldState != null && oldState.getName().equals(PlatformerGame.STATE_MENU))
         ((PlatformerWorld)world).setCurrentLevelIndex(0);

      // Either restore from pause or create level
      if(oldState != null && oldState.getName().equals(PlatformerGame.STATE_PAUSE))
      {
         // Restore stored game objects
         for(GameObject obj : mStoredGameObjects)
            world.add(obj);
      }
      else
      {
         // Reset coins
         ((PlatformerWorld) world).setCoins(0);
         // Retrieve level from world and create
         ((PlatformerWorld) world).getLevel().buildLevel(world);
      }

      // Remove stored game objects
      mStoredGameObjects.clear();
   }

   @Override
   public void exit(State newState, GameWorld world)
   {
      // Save game state in case we are pausing
      mStoredGameObjects.addAll(world.getGameObjects());

      // Empty world
      world.removeAll();

      // Return the camera to the original position
      Camera camera = world.getCamera();
      camera.position.set(camera.viewportWidth/2, camera.viewportHeight/2, 0);
   }
}
