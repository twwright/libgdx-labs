package com.fatcat.gdxlabs.projects.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.fatcat.gdxlabs.projects.asteroids.AsteroidsGame;
import com.fatcat.gdxlabs.projects.checkers.CheckersGame;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new CheckersGame(), config);
	}
}
